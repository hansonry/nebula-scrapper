drill_head_length=3;
drill_head_radius=1;
bit_length=1.5;
handle_length=2.5;
handle_radius=0.8;
handle_angle=10;
battery_size=2.3;

use <utils.scad>


drill_head_small_radius= drill_head_radius*0.7;
drill_head_front_length=drill_head_length * 0.7;
drill_head_rear_length=drill_head_length - drill_head_front_length;

module drill_head()

union() {
scale([1, 1, 0.9])
hull() {
translate([-drill_head_rear_length, 0, 0])
sphere(r=drill_head_small_radius,$fn=10);
sphere(r=drill_head_radius, $fn=10);
translate([drill_head_front_length, 0, 0])
sphere(r=drill_head_small_radius, $fn=10);
}

drill_clutch_length=drill_head_length * 0.3;
drill_clutch_radius=drill_head_radius * 0.4;

translate([drill_head_front_length + drill_head_small_radius * 0.5, 0 , 0])
union()
{

rotate([0, 90 , 0])
hull()
{
cylinder(h=drill_clutch_length * 0.8, r=drill_clutch_radius, $fn=8);
cylinder(h=drill_clutch_length, r=drill_clutch_radius * 0.5, $fn=8);
}

translate([drill_clutch_length, 0 , 0])
rotate([0, 90 , 0])
cylinder(h=bit_length, r=drill_clutch_radius*0.2,$fn=8);
}
}

drill_head();

translate([-sin(handle_angle) * handle_length / 2, 0, -(drill_head_radius * 0.4 + handle_length/2)])
rotate([0, handle_angle, 0])
union()
{
beveled_cube(handle_radius, handle_radius * 0.8, handle_length, handle_radius * 0.1,center=true);

translate([battery_size * 0.15 ,0,-handle_length /2])
rotate([0, -handle_angle, 0])
beveled_cube(battery_size, battery_size * 0.5, battery_size * 0.5, 0.1,center=true);

}