handle_length = 1.1;
handle_depth = 0.3;
bevel = 0.03;

head_length = 0.7;
head_size = 0.4;

handle_width = 0.7 * handle_depth;

// Side Handle
side_handle_big_radius = 0.15;
side_handle_small_radius = 0.08;
side_handle_legnth = 0.7;
side_handle_fn = 8;

// Head
center_head_length = head_length * 0.8;
top_length = center_head_length * .5;
top_size = head_size * 0.6;

// Blade
blade_gaurd_radius = 0.5;
blade_gaurd_depth = 0.1;
blade_gaurd_thickness= 0.02;

blade_depth = 0.02;
blade_radius = blade_gaurd_radius * 0.9;

// Battery
battery_width = 0.4;
battery_height = 0.4;
battery_depth = 0.6;
battery_angle = 45;

use <utils.scad>


// Handle
beveled_cube(handle_length, handle_width, handle_depth, bevel, true);

// Head
translate([(handle_length + head_length) /2 , 0, 0])
hull(){
beveled_cube(center_head_length, head_size, head_size, bevel, true);
    translate([-center_head_length /2, 0 ,0])
beveled_cube(center_head_length / 2, handle_width, handle_depth, bevel, true);
    translate([(top_length +center_head_length) /2, 0 ,-(head_size - top_size) / 2])
beveled_cube(top_length, top_size, top_size, bevel, true);
}

// Blade Gaurd and Blade
translate([handle_length /2 + head_length + top_length / 4, 0, -(blade_gaurd_depth + head_size /2 ) ])
union(){
difference() {
difference(){
    corners= 16;
cylinder(h=blade_gaurd_depth, r=blade_gaurd_radius,$fn=corners);
    translate([0, 0, -blade_gaurd_thickness])
cylinder(h=blade_gaurd_depth, r=blade_gaurd_radius - blade_gaurd_thickness ,$fn=corners);
}
translate([0, -(blade_gaurd_radius + 1) / 2, 0-.5])
cube([blade_gaurd_radius + 1,blade_gaurd_radius + 1, blade_gaurd_depth + 1]);
}
translate([0,0,blade_gaurd_depth / 2])
cylinder(h=blade_depth, r=blade_radius,$fn=16);
cylinder(h=blade_gaurd_depth, r=top_size * 0.4,$fn=8);

}

// Side Handle
translate([handle_length /2 + head_length / 2, -head_size /2 , 0])
rotate([90, 0, 0])
union() {
cylinder(h=side_handle_legnth, r=side_handle_small_radius, $fn=side_handle_fn);
cylinder(h=side_handle_legnth * 0.2, r=side_handle_big_radius, $fn=side_handle_fn);

}

// Battery
translate([-handle_length/2 * 0.9, 0, -battery_depth/5])
rotate([0, -battery_angle, 0])
beveled_cube(battery_width, battery_height, battery_depth, bevel, true);
