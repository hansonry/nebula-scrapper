handle_length=1.3;
handle_radius=0.2;
blade_length=1.5;
blade_radius=0.05;
blade_tip_size=0.02;

handle_sides=6;

// Handle


notch_at = handle_length * 0.94;
notch_heigth = handle_length * 0.05;

grip_cutter_offset=handle_radius * 1.0;
grip_cutter_radius= handle_radius * 0.4;
module grip_cutter() {
    
cylinder(h=handle_length + 1,r = grip_cutter_radius * 0.7, $fn=10);
}

tip_cutter_radius=blade_length*0.2;
module blade_cutter() {
    translate([0, blade_radius + 0.5, blade_length])
    scale([(blade_radius * 2) / (tip_cutter_radius * 2),1,1])
    rotate([90, 0, 0])
    cylinder(h=blade_radius * 2 + 1, r=tip_cutter_radius, $fn=16);
}

difference() {
union() {
difference() {
/// Rounded End
union() {
handle_rounded_end = handle_radius;
rest_of_handle=handle_length - handle_radius;
cylinder(h=handle_rounded_end / 2 ,r1 = handle_radius * 0.1, r2= handle_radius * 0.8 , $fn=handle_sides);
translate([0, 0, handle_rounded_end / 2])
cylinder(h=handle_rounded_end / 2 ,r1 = handle_radius * 0.8, r2= handle_radius , $fn=handle_sides);


/// Shaft
translate([0, 0, handle_radius])
cylinder(h=handle_length,r = handle_radius, $fn=handle_sides);
}

/// Notch
translate([0, 0, notch_at])
cylinder(h=notch_heigth,r = handle_radius + 1, $fn=handle_sides);

}

translate([0, 0, notch_at])
cylinder(h=notch_heigth,r = handle_radius * 0.7, $fn=handle_sides);
}
/// Grip
for (angle = [0 : 360/6 : 360]) {
    a = angle + 30;
    rotate([0, 0, a])
translate([grip_cutter_offset, 0,0])
grip_cutter();
}
}

// Shaft and Blade
translate([0, 0, handle_length])
difference() {
    cylinder(h=blade_length,r = blade_radius, $fn=handle_sides);

/// Blade Tip
    union() {
        move_amount = (blade_tip_size / 2) + blade_radius;
        translate([move_amount, 0, 0])
        blade_cutter();
        translate([-move_amount, 0, 0])
        blade_cutter();

    }
}

