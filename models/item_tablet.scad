width=1;
height=0.7;
depth=0.12;

radius= 0.04;

screen_depth=depth * 0.2;
screen_inset=0.2;

radius2 = radius * 2;

module tabletBody() {
minkowski() {

cube([width - radius2, height - radius2, depth - radius2], center = true);
    
sphere(r=radius, $fn=8);
}
}


module screenSubtractor(){
minkowski() {
    sd = screen_inset + screen_depth;
    cube([width - sd, height - sd, 0.01], center=true);
    union(){
    cylinder(h=screen_depth, r=screen_depth / 2, center=true, $fn=8);
    translate([0,0,screen_depth / 4])
    cylinder(h=screen_depth / 2, r1=screen_depth / 2, r2=screen_depth, center=true, $fn=8);
    }
}
}
difference() {
tabletBody();
translate([0, 0, (depth - (screen_depth * 0.8)) / 2 ])
screenSubtractor();
}
