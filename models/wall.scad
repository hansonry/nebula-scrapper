include <scale.scad>

frame_depth = big_lip / 2;
cover_depth = big_lip - frame_depth;

frame_width = big_size * 0.1;

frame_cut_width = big_size - frame_width * 2;

brace_width = frame_width * 0.9;
brace_depth = frame_depth * 0.7;


module frame()
union()
{
difference() {
    cube([big_size, big_size, frame_depth]);
    translate([frame_width, frame_width, -0.5])
    cube([frame_cut_width, frame_cut_width, big_size]);
}
translate([big_size /2, big_size / 2, brace_depth / 2 + (frame_depth - brace_depth)])
rotate([0, 0, 45])
 cube([big_size * sqrt(2) * 0.9, brace_width, brace_depth],center=true);
}

scale(1) {


frame();

translate([big_size * 2, 0, 0])
union() {
difference() {
hull() {
    edge_size = cover_depth;
    edge_cube = big_size - edge_size * 2;
cube([big_size, big_size, frame_depth]);
    translate([edge_size, edge_size, frame_depth])
cube([edge_cube, edge_cube, cover_depth]);
}
translate([-0.5, -0.5, -1])
cube([big_size + 1, big_size + 1, frame_depth + 1]);
}
frame();
}
}