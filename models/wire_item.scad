sides=8;



module spiral(angle, height, parts, material_radius, spiral_radius)
union() {
step_angle = angle / parts;
step_height = height / parts;
angle_jitter = step_angle * 0.15;
height_jitter = step_height * 0.15;
x=[ for (i = [0 : step_angle : angle]) cos(i + rands(-angle_jitter,angle_jitter,1)[0]) * spiral_radius ]; 
y=[ for (i = [0 : step_angle : angle]) sin(i + rands(-angle_jitter,angle_jitter,1)[0]) * spiral_radius  ]; 
z=[ for (i = [0 : step_height : height]) i + rands(-height_jitter,height_jitter,1)[0] ]; 
    

for (i=[0:parts - 1]){
    hull()
    {
        translate([x[i], y[i], z[i]])
        sphere(r=material_radius,$fn=sides);
        translate([x[i + 1], y[i + 1], z[i + 1]])
        sphere(r=material_radius, $fn=sides);
    }
}
}
//wire_curve();
turns = 3;
height=turns * 0.2;
spiral(angle=turns * 360, parts=turns * 8, height=height ,material_radius=0.15, spiral_radius=1);

translate([1, 0, 0])
rotate([90, 0, -5])
cylinder(h = 0.5, r=0.08,$fn=sides);
translate([1, 0, height])
rotate([-90, 0, 5])
cylinder(h = 0.5, r=0.08,$fn=sides);