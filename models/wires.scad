sides=8;

module wire_strait()
cylinder(h=200,r=10,center=true,$fn=sides);
module wire_curve()
rotate([90, 0, 0])
translate([-100, -100, 0])
rotate_extrude(angle=90,$fn=24) {
    translate([100, 0, 0])
    circle(r=10,$fn=sides);
}

spacing=400;
scaling=0.005;
//scaling=1;

scale([scaling, scaling, scaling]) 
{
// Strait
translate([0, 0 * spacing, 0])
wire_strait();
    
// Curve
translate([0, 1 * spacing, 0])
wire_curve();
    
// T
translate([0, 2 * spacing, 0])
{
    wire_strait();
    wire_curve();    
    rotate([180, 0, 0]) wire_curve();
}


// +
translate([0, 3 * spacing, 0])
{
    wire_curve();    
    rotate([0, 90, 0]) wire_curve();
    rotate([0, 180, 0]) wire_curve();
    rotate([0, 270, 0]) wire_curve();
}

// 3 way
translate([0, 4 * spacing, 0])
{
    rotate([90, 0, 0]) wire_curve();    
    rotate([0, 0, 270]) wire_curve();
    rotate([0, 0, 0]) wire_curve();
}


// + and a side
translate([0, 5 * spacing, 0])
{
    wire_curve();    
    rotate([0, 90, 0]) wire_curve();
    rotate([0, 180, 0]) wire_curve();
    rotate([0, 270, 0]) wire_curve();
    
    rotate([90, 0, 0]) wire_curve();
    rotate([90, 90, 0]) wire_curve();
    rotate([90, 180, 0]) wire_curve();
    rotate([90, 270, 0]) wire_curve();
}


// All Directions
translate([0, 6 * spacing, 0])
{
    wire_curve();    
    rotate([0, 90, 0]) wire_curve();
    rotate([0, 180, 0]) wire_curve();
    rotate([0, 270, 0]) wire_curve();
    
    rotate([90, 0, 0]) wire_curve();
    rotate([90, 90, 0]) wire_curve();
    rotate([90, 180, 0]) wire_curve();
    rotate([90, 270, 0]) wire_curve();

    rotate([270, 0, 0]) wire_curve();
    rotate([270, 90, 0]) wire_curve();
    rotate([270, 180, 0]) wire_curve();
    rotate([270, 270, 0]) wire_curve();

}


}
