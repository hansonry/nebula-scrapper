module beveled_cube(w, d, h, bevel,center=false)
{
    if(center)
    hull()
    {
        db = bevel * 2;
        
        cube([w - db,d - db, h],center=center);
        cube([w,     d - db, h - db],center=center);
        cube([w - db,d,      h - db],center=center);
    }
    else
    hull()
    {
        db = bevel * 2;
        
        translate([bevel, bevel, 0])
        cube([w - db,d - db, h],center=center);
        translate([0, bevel, bevel])
        cube([w,     d - db, h - db],center=center);
        translate([bevel, 0, bevel])
        cube([w - db,d,      h - db],center=center);
    }
}
beveled_cube(1,1,1, .1);
