part_width=1;
part_depth=0.7;
part_height=0.02;


module single_sheet() {

cube([part_width, part_depth, part_height], center=true);
}

delta_height = part_height - part_height / 100;

shift= part_height / 2;

single_sheet();
translate([shift,shift, delta_height])
single_sheet();

translate([0,shift, -delta_height])
single_sheet();