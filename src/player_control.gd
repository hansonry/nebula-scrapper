extends Node

@export var character              : NodePath
@export var object_picker          : NodePath
@export var tool_tip               : NodePath
@export var item_spawn_drop_target : NodePath
@export var throw_power            : NodePath
@export var atmos_ui               : NodePath
@export var console                : NodePath

@export_group("Item Slot UIs")
@export var ui_hands  : NodePath

const SPEED = 7.0
const THROW_CHARGE_TIME = 1
const DROP_POWER = 0.2

var _character_node : Character = null
@onready var _object_picker_node : ObjectPicker = get_node(object_picker)
@onready var _tool_tip_node: Control = get_node(tool_tip)
@onready var _item_spawn_drop_target_node : Control = get_node(item_spawn_drop_target)
@onready var _ui_hands_node : Hands = get_node(ui_hands)
@onready var _throw_power_node : ProgressBar = get_node(throw_power)
@onready var _atmos_ui_node : AtmosInfo = get_node(atmos_ui)
@onready var _console_node : Console = get_node(console)


var _mouse_captured : bool = true
var _mouse_click_length : float = -1

var _placement_node : Node3D = null

var _manual_step_counter: int = 0

func _register_console_commands():
	_console_node.add_command("exit", _exit_console_callback, "Exits the game in an unsafe way")
	_console_node.add_command("gas_add", _gas_add_console_callback, "Adds Gas")
	_console_node.add_command("atmos_pause", _atmos_pause_console_callback, "Pauses Atmos Simulation")
	_console_node.add_command("atmos_continue", _atmos_continue_console_callback, "Continues Atmos Simulation")
	_console_node.add_command("atmos_step", _atmos_step_console_callback, "Runs one step of Atmos Simulation")
	

func _ready():
	get_parent().ready.connect(_on_parent_ready)
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	_mouse_captured = true
	_register_console_commands()

func _on_parent_ready():
	_tool_tip_node.hide()
	_throw_power_node.hide()
	_throw_power_node.value = 0
	set_controlled_character(get_node(character))
	_object_picker_node.item_entered.connect(_picker_item_entered)
	_object_picker_node.item_exited.connect(_picker_item_exited)
	_item_spawn_drop_target_node.set_drag_forwarding(_get_drag_data, _can_drop_data, _drop_data)


func set_controlled_character(next_character: Character):
	# TODO: Move HeadBoneAttachment
	if _character_node != null:
		_character_node.active_hand_item_change.disconnect(_item_in_active_hand_changed)
		_object_picker_node.remove_exception(_character_node)
	_ui_hands_node.set_character(next_character)
	_character_node = next_character
	_character_node.active_hand_item_change.connect(_item_in_active_hand_changed)
	_object_picker_node.add_exception(_character_node)

func _item_in_active_hand_changed():
	if _placement_node != null:
		_placement_node.hide()
		_placement_node = null

func _drop_data(_at_position : Vector2, data):
	if not Engine.is_editor_hint():
		var result = _character_node.find_item_stack(data.stack)
		assert(result != null)
		assert(result.inventory == _character_node.inv_hands)
		_character_node.throw_item(DROP_POWER, result.index)
		

func _get_drag_data(_at_position):
	if not Engine.is_editor_hint():
		if _object_picker_node.selected != null and _object_picker_node.selected is ItemNode:
			var selected_item_node : ItemNode = _object_picker_node.selected
			var stack : ItemStack = selected_item_node.item_stack
			return stack.get_drag_info(_item_spawn_drop_target_node, selected_item_node)
	return null


func _can_drop_data(_at_position: Vector2, data) -> bool:
	if Engine.is_editor_hint() or typeof(data) != TYPE_DICTIONARY or not data.has("type") or data.source is ItemNode:
		return false
	if data.type == "ItemStack":
		return true
	return false


func _attempt_to_pickup(obj : ItemNode):
	#print("Attempt to pickup ", obj)
	_character_node.pickup_item(obj)
	if obj.is_queued_for_deletion():
		_object_picker_node.clear_selected_item()


func _use_item_on_node(node: Node3D) -> bool:
	if node is BuildableNode:
		var buildable_node :BuildableNode = node
		if _character_node.assemble_node(buildable_node):
			return true
		if _character_node.disassemble_node(buildable_node):
			return true
	return false


func _unhandled_input(event):
	# Mouse in viewport coordinates.
	if event is InputEventMouseButton:
		#print("Mouse Click/Unclick at: ", event.position)
		match event.button_index:
			MouseButton.MOUSE_BUTTON_LEFT:
				if _placement_node == null:
					if _object_picker_node.selected != null:
						if _object_picker_node.selected is ItemNode:
							var selected_item_node : ItemNode = _object_picker_node.selected
							if event.pressed:
								_mouse_click_length = 0
							elif _mouse_click_length < 0.5:
								_attempt_to_pickup(selected_item_node)
						if _object_picker_node.selected is BuildableNode:
							_use_item_on_node(_object_picker_node.selected)
							if _object_picker_node.selected.is_queued_for_deletion():
								_object_picker_node.clear_selected_item()
				else:
					if event.pressed and _placement_node.highlight != Placement.Highlight.RED:
						_placement_node.place()
						_placement_node = null
			MouseButton.MOUSE_BUTTON_RIGHT:
				var inv_index : Character.Hand = _character_node.active_hand
				if event.pressed:
					if _placement_node == null:
						if  _character_node.inv_hands.has_stack(inv_index):
							var stack = _character_node.inv_hands.get_item_stack(inv_index)
							if stack.item_type.buildables.size() > 0:
								print("Attempt to start a build")
								_placement_node = _character_node.find_vessel().get_node("Placement")
								assert(_placement_node != null)
								_placement_node.character = _character_node
								_placement_node.buildables = stack.item_type.buildables
								_placement_node.position = _character_node.position
								_placement_node.object_picker = _object_picker_node
								_placement_node.show()
								
					else:
						_placement_node.hide()
						_placement_node = null
					
	elif event is InputEventMouseMotion:
		#print("Mouse Motion at: ", event.position)
		if _mouse_captured:
			_character_node.target_facing.y -= event.relative.x * 0.005
			_character_node.target_facing.x -= event.relative.y * 0.005
			#print(_character.target_facing.x * 180 / PI)
			var max_angle = PI / 2
			_character_node.target_facing.x = clamp(_character_node.target_facing.x, -max_angle, max_angle)

func _process_mouse_mode(delta):
	if Input.is_action_just_pressed("MouseRelease"):
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
		_mouse_captured = false
		
	elif Input.is_action_just_released("MouseRelease"):
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
		_mouse_captured = true


func _process_movement(delta):
	
	# Jump
	if Input.is_action_just_pressed("Jump"):
		_character_node.jump()
	
		
	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var target_velocity : Vector3 = Vector3.ZERO
	var input_dir = Input.get_vector("Left", "Right", "Forward", "Backward")
	var direction = (_character_node.transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		target_velocity.x = direction.x * SPEED
		target_velocity.z = direction.z * SPEED
	else:
		target_velocity.x = 0
		target_velocity.z = 0
		
	_character_node.target_velocity = target_velocity
	
func _picker_item_entered(obj: Node3D):
	if obj is ItemNode:
		var item : ItemNode = obj
		_tool_tip_node.show_stack_text(item.item_stack)
		item.highlighted = true
	elif obj is BuildableNode:
		_character_node.active_hand_item_target_override(obj)



func _picker_item_exited(obj: Node3D):
	if obj is ItemNode:
		var item : ItemNode = obj
		_tool_tip_node.hide()
		item.highlighted = false
	elif obj is BuildableNode:
		_character_node.active_hand_item_target_override(null)


func _process_throw(delta):
	var index : Character.Hand = _character_node.active_hand
	if Input.is_action_just_released("ThrowItem") and _character_node.inv_hands.has_stack(index):
		var l_throw_power: float = lerp(DROP_POWER, 1.0, _throw_power_node.value)
		_character_node.throw_item(l_throw_power)
		
	if Input.is_action_pressed("ThrowItem") and _character_node.inv_hands.has_stack(index):
		_throw_power_node.show()
		_throw_power_node.value += delta / THROW_CHARGE_TIME
	else:
		_throw_power_node.value = 0
		_throw_power_node.hide()



func _find_closes_big_pos(pos: Vector3) -> Vector3:
	var big_position: Vector3i = BigMap.big_position_from_local(pos)
	return big_position * BigMap.GRID_SIZE

func _process_placement(delta):
	if _placement_node != null:
		var rot = Quaternion.from_euler(_character_node.target_facing)
		var offset = rot * Vector3(0, 0, -4)
		#print(offset)
		_placement_node.position = _find_closes_big_pos(offset + _character_node.position + Vector3(0, BigMap.GRID_SIZE / 2, 0))
		if Input.is_action_just_pressed("RotatePlacement"):
			_placement_node.orientation_next()
			#print(BigMap.Slot.keys()[_placement_node.facing] )


func _process_hands(delta):
	if Input.is_action_just_pressed("SwitchHands"):
		_character_node.switch_active_hand()


func _process_atmos_ui(delta):
	var sample: AtmosSample = _character_node.get_atmos_sample()
	_atmos_ui_node.set_sample(sample)

func _process_pause(delta):
	if Input.is_action_just_released("ui_cancel"):
		%PauseMenu.open()


func _process(delta):
	_process_mouse_mode(delta)
	if not _console_node.open:
		_process_movement(delta)
		_process_hands(delta)
		_process_throw(delta)
		_process_pause(delta)
		
	_process_atmos_ui(delta)
	if _mouse_click_length >= 0:
		_mouse_click_length += delta
	

func _exit_console_callback(args):
	get_tree().quit()

func _gas_add_console_callback(args):
	# Since this is a test function, no need to go though the player interface
	GameAtmosSystem.queue_callable(func(atmos_system :AtmosSystem):
		var volume: AtmosVolume = GameAtmosSystem.get_volume_at(_character_node.global_position)
		volume.add_mols({"Oxygen": 500 }, 293.15)
		_console_node.write("Mols Added")
		)

func _atmos_pause_console_callback(args):
	GameAtmosSystem.paused = true
	_console_node.write("Atmos System Paused")


func _atmos_continue_console_callback(args):
	GameAtmosSystem.paused = false
	_console_node.write("Atmos System Continued")

func _atmos_step_console_callback(args):
	GameAtmosSystem.paused = true
	GameAtmosSystem.manual_step()
	_console_node.write("Atmos System Stepped %d" % _manual_step_counter)
	_manual_step_counter += 1
