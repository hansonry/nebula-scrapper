extends PanelContainer


@onready var _label : Label = $Label

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func show_text(text):
	_label.text = text
	show()
	

func show_stack_text(stack: ItemStack):
	var text : String = stack.item_type.name
	if stack.item_type.max_stack > 1:
		text = text + " %d" % stack.count
	show_text(text)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if is_visible():
		position = get_viewport().get_mouse_position() + Vector2(0, -size.y * 2)
		

