extends PanelContainer
class_name PauseMenu



# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	visible = false
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if Input.is_action_just_released("ui_cancel"):
		_unpause()

func _internal_open():
	get_tree().paused = true
	visible = true
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE


func open():
	_internal_open.call_deferred()

func _unpause():
	visible = false
	get_tree().paused = false
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func _exit_game():
	get_tree().quit()


func _on_continue_button_pressed() -> void:
	_unpause()



func _on_exit_button_pressed() -> void:
	_exit_game()
