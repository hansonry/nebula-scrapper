extends RigidBody3D
class_name Vessel


var _big_map: BigMap = null
var _small_map: SmallMap = null
var _pipe_manager: ExternalConnectorNetworkManager = null
var _atmos_pipe : AtmosPipes = null

# Called when the node enters the scene tree for the first time.
func _ready():
	_big_map = BigMap.new()
	_small_map = SmallMap.new()
	_pipe_manager = ExternalConnectorNetworkManager.new(ConnectorNetwork.Type.PIPE)
	_atmos_pipe = AtmosPipes.new(_pipe_manager)
 

func has_big_slot(big_position: Vector3i, slot : BigMap.Slot) -> bool:
	return _big_map.has_slot(big_position, slot)
	

func is_big_slot_blocked(big_position: Vector3i, slot : BigMap.Slot, type: BigBuildable.Type) -> bool:
	return _big_map.is_slot_blocked(big_position, slot, type)
	
func get_big_slot(big_position: Vector3i, slot : BigMap.Slot) -> BigBuildableNode:
	return _big_map.get_slot(big_position, slot)
	



func set_big_slot(big_position: Vector3i, slot : BigMap.Slot, node: BigBuildableNode):
	_big_map.set_slot(big_position, slot, node)

func clear_big_slot(big_position: Vector3i, slot : BigMap.Slot):
	_big_map.clear_slot(big_position, slot)

func get_big_slot_atmos_link(big_position: Vector3i, slot : BigMap.Slot) -> AtmosLink:
	return _big_map.get_slot_atmos_link(big_position, slot)



func set_small_node(small_position: Vector3i, layer: SmallMap.Layer, node: SmallBuildableNode):
	_small_map.set_node(small_position, layer, node)
	if node.has_network_type(ConnectorNetwork.Type.PIPE):
		_pipe_manager.add(node)

func has_small_node(small_position: Vector3i, layer: SmallMap.Layer) -> bool:
	return _small_map.has_node(small_position, layer)

func clear_small_node(small_position: Vector3i, layer: SmallMap.Layer):
	var node: SmallBuildableNode = _small_map.get_node(small_position, layer)
	if node.has_network_type(ConnectorNetwork.Type.PIPE) and node != null:
		_pipe_manager.remove(node)
	_small_map.clear_node(small_position, layer)

func has_small_connection_with(small_position: Vector3i, network_set : ConnectorNetworkSet) -> bool:
	return _small_map.has_connection_with(small_position, network_set)

func get_small_connection_with_count(small_position: Vector3i, network_set : ConnectorNetworkSet) -> int:
	return _small_map.get_connection_with_count(small_position, network_set)


func get_pipe_volume_from_network(network: ExternalConnectorNetwork) -> AtmosVolume:
	return _atmos_pipe.get_volume_from_network(network)

func get_pipe_information(node: SmallBuildableNode) -> Dictionary:
	var network : ExternalConnectorNetwork = _pipe_manager.find_network_containing(node)
	var volume : AtmosVolume = null
	if network != null:
		volume = _atmos_pipe.get_volume_from_network(network)
	return {
		&"network": network,
		&"volume" : volume
	}
	

func get_external_network(node: SmallBuildableNode, network_name: StringName) -> ExternalConnectorNetwork:
	var network: ConnectorNetwork = node.network_set.get_network_by_name(network_name)
	assert(network != null)
	if network.type == ConnectorNetwork.Type.PIPE:
		return _pipe_manager.find_network_containing(node)
	return null

func _exit_tree():
	_big_map.detach()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_big_map.transform = global_transform

static func find(node: Node3D) -> Vessel:
	while node != null and not node is Vessel:
		node = node.get_parent_node_3d()
	return node
