@tool
extends RigidBody3D
class_name Character

signal hand_switched()
signal active_hand_item_change

const JUMP_IMPULSE = 5

const MAX_MOVE_IMPULSE = 1

const MAX_ITEM_THROW_SPEED = 9

const MAX_ITEM_REACH = 5

enum Hand {LEFT, RIGHT}


const HAND_ANIMATION_SPEED: float = 2

@export var active_hand: Hand = Hand.LEFT:
	set(value):
		if value != active_hand:
			active_hand = value
			_update_up_hand()
			hand_switched.emit()
			active_hand_item_change.emit()

@export var target_velocity: Vector3 = Vector3.ZERO :
	set(value):
		target_velocity = value
		

@export var target_facing: Vector3 = Vector3.ZERO

@export_group("Inventory")

var inv_hands : InventoryGroup = InventoryGroup.new(Hand.size())

var _hand_stacks : Array[ItemStack] = []

var _inv_all : Array[InventoryGroup] = [inv_hands]

@export var i_hand_left  : ItemStack:
	set(value):
		inv_hands.set_item_stack(Hand.LEFT, value)
	get:
		return inv_hands.get_item_stack(Hand.LEFT)
		
@export var i_hand_right : ItemStack:
	set(value):
		inv_hands.set_item_stack(Hand.RIGHT, value)
	get:
		return inv_hands.get_item_stack(Hand.RIGHT)


var _pid : Pid3D = Pid3D.new(1, 0.1, 0.1)

var _try_jump: bool = false

var _is_on_floor: bool = true

@onready var _skelleton : Skeleton3D = $Skeleton3D
@onready var _head_bone_id : int = _skelleton.find_bone("Bone.004")

@onready var _left_hand_bone  : BoneAttachment3D = $Skeleton3D/LeftHandBoneAttachment3D
@onready var _right_hand_bone : BoneAttachment3D = $Skeleton3D/RightHandBoneAttachment3D
@onready var _right_hand_ik : SkeletonIK3D = $Skeleton3D/RightHandIK
@onready var _left_hand_ik : SkeletonIK3D = $Skeleton3D/LeftHandIK
@onready var _hand_ik_pivot_node : Node3D = $HandIKPivot

var _bone_array : Array[BoneAttachment3D] = []
var _item_hand_view : Array[ItemViewNode] = []
const _hand_animation_names : Array[StringName] = [&"hand_left/hand_up", &"hand_right/hand_up"]
@onready var _atmos_node : AtmosNode = $AtmosNode
@onready var _animation_player_right : AnimationPlayer = $RightAnimationPlayer
@onready var _animation_player_left : AnimationPlayer = $LeftAnimationPlayer
var _animation_player_list : Array[AnimationPlayer]

# Called when the node enters the scene tree for the first time.
func _ready():
	_bone_array.resize(Hand.size())
	_bone_array[Hand.LEFT] = _left_hand_bone
	_bone_array[Hand.RIGHT] = _right_hand_bone
	
	_animation_player_list.resize(Hand.size())
	_animation_player_list[Hand.LEFT] = _animation_player_left
	_animation_player_list[Hand.RIGHT] = _animation_player_right

	_hand_stacks.resize(Hand.size())
	_item_hand_view.resize(Hand.size())
	
	inv_hands.slot_changed.connect(_hands_slot_changed)
	_set_hand_based_on_stack(Hand.LEFT)
	_set_hand_based_on_stack(Hand.RIGHT)
	_right_hand_ik.start()
	_left_hand_ik.start()

func _set_hand_based_on_stack(index: int):
	var current_stack : ItemStack = inv_hands.get_item_stack(index)
	if current_stack != null:
		_item_hand_view[index] = current_stack.create_view_node()
		_item_hand_view[index].set_in_hand(index)
		_bone_array[index].add_child(_item_hand_view[index])
		if current_stack.item_type.has_tag(&"tool") and index == active_hand:
			_play_hand_animation(index)
	_hand_stacks[index] = current_stack

func _hands_slot_changed(index: int):
	var current_stack : ItemStack = inv_hands.get_item_stack(index)
	if current_stack != _hand_stacks[index]:
		if (current_stack == null or not current_stack.item_type.has_tag(&"tool")) and (_item_hand_view[index] != null and _item_hand_view[index].item_stack.item_type.has_tag(&"tool") and active_hand == index):
			_play_hand_animation(index, true)
		if _item_hand_view[index] != null:
			_item_hand_view[index].queue_free()
			_item_hand_view[index] = null
		_set_hand_based_on_stack(index)

func _update_up_hand():
	var new_active_hand : Hand = active_hand
	var prev_active_hand: Hand = get_other_hand(active_hand)
	var prev_active_stack : ItemStack = inv_hands.get_item_stack(prev_active_hand)
	var new_active_stack : ItemStack = inv_hands.get_item_stack(new_active_hand)
	if prev_active_stack != null and prev_active_stack.item_type.has_tag(&"tool"):
		_play_hand_animation(prev_active_hand, true)
	if new_active_stack != null and new_active_stack.item_type.has_tag(&"tool"):
		_play_hand_animation(new_active_hand)
	

func _play_hand_animation(index: int, backwards : bool = false):
	if backwards:
		_animation_player_list[index].play(_hand_animation_names[index], -1, -HAND_ANIMATION_SPEED, true)
	else:
		_animation_player_list[index].play(_hand_animation_names[index], -1, HAND_ANIMATION_SPEED)

func jump():
	if not Engine.is_editor_hint():
		_try_jump = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func is_on_floor() -> bool:
	return _is_on_floor


func get_other_hand(hand: Hand) -> Hand:
	if hand == Hand.LEFT:
		return Hand.RIGHT
	return Hand.LEFT

func switch_active_hand():
	active_hand = get_other_hand(active_hand)

func throw_item(power: float, hand: Hand = active_hand):
	var speed = power * MAX_ITEM_THROW_SPEED
	var stack : ItemStack = inv_hands.set_item_stack(hand, null)
	active_hand_item_change.emit()
	var item_node : ItemNode = stack.create_item_node()
	add_sibling(item_node)
	magic_throw_item(item_node, speed)

func pickup_item(item_node :ItemNode, into_hand : Hand = active_hand) -> int:
	if global_position.distance_to(item_node.global_position) < MAX_ITEM_REACH:
		if not inv_hands.has_stack(into_hand):
			var xfer_amount : int = item_node.item_stack.count
			inv_hands.set_item_stack(into_hand, item_node.item_stack)
			item_node.kill_self()
			if into_hand == active_hand:
				active_hand_item_change.emit()
			return xfer_amount
		else:
			var hand_stack: ItemStack = inv_hands.get_item_stack(into_hand)
			if not hand_stack.is_full() and hand_stack.item_type == item_node.item_stack.item_type:
				var xfer_amount : int = min(hand_stack.free_space(), item_node.item_stack.count)
				item_node.remove_from_stack(xfer_amount)
				hand_stack.count += xfer_amount
				if into_hand == active_hand:
					active_hand_item_change.emit()
				return xfer_amount
	return 0

func move_item_into_other_hand(src_hand: Hand) -> bool:
	if not inv_hands.has_stack(src_hand):
		return false
	var dest_hand: Hand = get_other_hand(src_hand)
	var src_stack : ItemStack = inv_hands.get_item_stack(src_hand)
	var dest_stack: ItemStack = inv_hands.get_item_stack(dest_hand)
	if dest_stack == null or src_stack.item_type != dest_stack.item_type:
		inv_hands.set_item_stack(dest_hand, src_stack)
		inv_hands.set_item_stack(src_hand, dest_stack)
		return true
	if dest_stack.is_full():
		return false
	var xfer_amount : int = min(dest_stack.free_space(), src_stack.count)
	if xfer_amount < src_stack.count:
		src_stack.count -= xfer_amount
	else:
		inv_hands.set_item_stack(dest_hand, null)
	dest_stack.count += xfer_amount
	active_hand_item_change.emit()
	return true

func magic_throw_item(item, speed:float):
	if not Engine.is_editor_hint():
		var g_position = to_global(Vector3(0, 2, -1.2))
		var g_to_l_position = item.get_parent().to_local(g_position)
		
		var target_basis = Basis.from_euler(target_facing)
		var velocity = (target_basis * Vector3(0, 0, -1)).normalized() * speed
		
		item.position = g_to_l_position
		item.linear_velocity = velocity

func _split_from_hand(hand: Hand, amount : int) -> ItemStack:
	var item_stack: ItemStack = inv_hands.get_item_stack(hand)
	if item_stack.count < amount:
		return null
	if item_stack.count == amount:
		inv_hands.set_item_stack(hand, null)
		if hand == active_hand:
			active_hand_item_change.emit()
		return item_stack
	if hand == active_hand:
		active_hand_item_change.emit()
	return item_stack.split(amount)

func build_big(big_position: Vector3i, design_index: int, slot: BigMap.Slot) -> Node3D:
	var item_stack: ItemStack = inv_hands.get_item_stack(active_hand)
	assert(design_index >= 0 and design_index < item_stack.item_type.buildables.size())
	var buildable: Buildable = item_stack.item_type.buildables[design_index]
	var vessel : Vessel = find_vessel()
	assert(vessel != null)
	if item_stack != null and buildable != null and vessel != null:
		var needed_matrial_amount : int = buildable.step_list[0].needed_material_amount
		var node : BigBuildableNode = BigBuildableNode.new()
		node.buildable = buildable
		node.set_inital_items(_split_from_hand(active_hand, needed_matrial_amount))
		node.big_position = big_position
		node.slot = slot
		
		node.transform = BigMap.make_transform(big_position, slot)
		vessel.add_child(node)
		return node
	return null

func build_small(small_position: Vector3i, a_basis: Basis, design_index: int) -> Node3D:
	var item_stack: ItemStack = inv_hands.get_item_stack(active_hand)
	assert(design_index >= 0 and design_index < item_stack.item_type.buildables.size())
	var buildable: Buildable = item_stack.item_type.buildables[design_index]
	var vessel : Vessel = find_vessel()
	assert(vessel != null)
	if item_stack != null and buildable != null and vessel != null:
		var needed_matrial_amount : int = buildable.step_list[0].needed_material_amount
		var node : SmallBuildableNode = SmallBuildableNode.new()
		node.buildable = buildable
		node.set_inital_items(_split_from_hand(active_hand, needed_matrial_amount))
		node.small_position = small_position
		
		node.transform = SmallMap.make_transform(small_position, a_basis)
		vessel.add_child(node)
		return node
	return null

func _try_to_add_item_to_hand(hand: Hand, item_stack: ItemStack) -> bool:
	var hand_item_stack : ItemStack = inv_hands.get_item_stack(hand)
	if hand_item_stack == null:
		inv_hands.set_item_stack(hand, item_stack)
		if hand == active_hand:
			active_hand_item_change.emit()
		return true
	if (item_stack.item_type == hand_item_stack.item_type and 
		item_stack.count + hand_item_stack.count <= item_stack.item_type.max_stack):
		hand_item_stack.count += item_stack.count
		if hand == active_hand:
			active_hand_item_change.emit()
		return true
	return false

func _spawn_drop(drop: ItemStack) -> bool:
	if _try_to_add_item_to_hand(Hand.LEFT, drop):
		return true
	if _try_to_add_item_to_hand(Hand.RIGHT, drop):
		return true
	var item_node : ItemNode = drop.create_item_node()
	add_sibling(item_node)
	magic_throw_item(item_node, 0)
	return false

func assemble_node(buildable_node: BuildableNode) -> bool:
	var active_hand_item_stack: ItemStack = inv_hands.get_item_stack(active_hand)
	var other_hand_item_stack: ItemStack = inv_hands.get_item_stack(get_other_hand(active_hand))
	var build_step : BuildStep = buildable_node.get_next_step()
	if build_step != null and build_step.can_assemble(active_hand_item_stack, other_hand_item_stack):
		var material_hand: Hand = get_other_hand(active_hand) if build_step.needs_tool_to_assemble() else active_hand
		buildable_node.assemble(_split_from_hand(material_hand, build_step.needed_material_amount))
		return true
	return false

		

func disassemble_node(buildable_node: BuildableNode) -> bool:
	var item_stack: ItemStack = inv_hands.get_item_stack(active_hand)
	if item_stack != null:
		var build_step : BuildStep = buildable_node.get_current_step()
		if  TagUtil.is_at_least_one_tag_satisfied(build_step.disassemble_tool, item_stack.item_type.tags):
			var dropped_stack : ItemStack = buildable_node.disassemble()
			_spawn_drop(dropped_stack)
			return true
	return false


func _physics_process(delta):
	if not Engine.is_editor_hint():
		var bone_rot = _skelleton.get_bone_pose_rotation(_head_bone_id).get_euler()
		bone_rot.x = rotate_toward(bone_rot.x, target_facing.x, delta * 5)
		rotation.y = rotate_toward(rotation.y, target_facing.y, delta * 5)
		#rotation.z = rotate_toward(rotation.z, target_facing.z, delta * 5)
		_hand_ik_pivot_node.basis = Basis.from_euler( Vector3(target_facing.x - deg_to_rad(10), 0 ,0))
		
		_skelleton.set_bone_pose_rotation(_head_bone_id, Quaternion.from_euler(bone_rot))
			
		# Handle jump.
		var jump_impulse : Vector3 = Vector3.ZERO
		if _try_jump and is_on_floor():
			jump_impulse.y = JUMP_IMPULSE
			_try_jump = false
		
		var target_agree_with_grav = target_velocity
		target_agree_with_grav.y = linear_velocity.y
		var desired_command = _pid.update_target(linear_velocity, target_agree_with_grav, delta)
		if desired_command.length_squared() > 1:
			desired_command = desired_command.normalized()
		var desired_impulse = desired_command * MAX_MOVE_IMPULSE
		#print(desired_impulse)
		apply_central_impulse(desired_impulse + jump_impulse)


func active_hand_item_target_override(node :BuildableNode):
	var active_item : ItemViewNode = _item_hand_view[active_hand]
	if active_item != null and active_item.has_method("target_override"):
		active_item.target_override(node)
		
	


func find_vessel() -> Vessel:
	var node : Node3D = self
	while node != null and not node is Vessel:
		node = node.get_parent()
	return node

func find_item_stack(stack :ItemStack):
	for inv_group :InventoryGroup in _inv_all:
		var index : int = inv_group.find_index_of_item_stack(stack)
		if index >= 0:
			return {inventory = inv_group, index = index}
	return null

func get_atmos_sample() -> AtmosSample:
	return _atmos_node.sample
