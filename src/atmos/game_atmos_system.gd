extends Node


const ATMOS_STEP_TIMEOUT = 0.25
var _step_timer :float = 0

var _thread: Thread
var _thread_running: bool
var _semaphore: Semaphore
var _run_counter : int = 0
var _thread_run_counter : int = 0
var paused : bool = false 
var _big_map_list : Array[BigMap]

var _atmos_system : AtmosSystem

var _callback_queue: Array[Callable]
var _callback_queue_mutex: Mutex

var atmos_step_before : ThreadSafeSignal  = ThreadSafeSignal.new()
var atmos_step_after : ThreadSafeSignal   = ThreadSafeSignal.new()

var _mutex : Mutex 


func _ready():
	_atmos_system = AtmosSystem.new(preload("res://atmos/atmos_config.tres"))
	_thread = Thread.new()
	_mutex = Mutex.new()
	_callback_queue_mutex = Mutex.new()
	_semaphore = Semaphore.new()
	# You can bind multiple arguments to a function Callable.
	_thread_running = true
	_thread.start(_thread_function)

func _thread_function():
	while _thread_running:
		while _thread_run_counter == _run_counter and _thread_running:
			_semaphore.wait()
		if _thread_running:
			_step()
			_thread_run_counter += 1

func _tell_atmos_to_step():
	_run_counter += 1
	_semaphore.post()

func manual_step():
	_tell_atmos_to_step()
	
	

func _process(delta):
	if not paused:
		_step_timer += delta
	if _step_timer >= ATMOS_STEP_TIMEOUT:
		_step_timer -= ATMOS_STEP_TIMEOUT
		_tell_atmos_to_step()
		
		
func _exit_tree():
	_thread_running = false
	_semaphore.post()
	_thread.wait_to_finish()
	
func _step():
	_mutex.lock()
	atmos_step_before.signal_emit()
	_atmos_system.step()
	atmos_step_after.signal_emit()
	_callback_queue_mutex.lock()
	while _callback_queue.size() > 0:
		var callable : Callable = _callback_queue.pop_front()
		_callback_queue_mutex.unlock()
		callable.call(_atmos_system)
		_callback_queue_mutex.lock()
	_callback_queue_mutex.unlock()
	_mutex.unlock()

func add_grid_base(base: BigMap):
	_mutex.lock()
	_big_map_list.append(base)
	_mutex.unlock()

func remove_grid_base(base: BigMap):
	_mutex.lock()
	var index: int = _big_map_list.find(base)
	_big_map_list.remove_at(index)
	_mutex.unlock()


func queue_callable(callback: Callable):
	if _mutex.try_lock():
		callback.call(_atmos_system)
		_mutex.unlock()
	else:
		_callback_queue_mutex.lock()
		_callback_queue.push_back(callback)
		_callback_queue_mutex.unlock()

func get_sample_at(l_global_position: Vector3, sample: AtmosSample = null) -> AtmosSample:
	_mutex.lock()
	if sample == null:
		sample = AtmosSample.new()
	for big_map: BigMap in _big_map_list:
		if big_map.contains_global_point(l_global_position):
			var out : AtmosSample = big_map.get_sample_at_global_position(l_global_position, sample)
			_mutex.unlock()
			return out
	_mutex.unlock()
	return null

func get_volume_at(l_global_position: Vector3) -> AtmosVolume:
	_mutex.lock()
	for big_map: BigMap in _big_map_list:
		if big_map.contains_global_point(l_global_position):
			var out : AtmosVolume = big_map.get_volume_at_global_position(l_global_position)
			_mutex.unlock()
			return out
	_mutex.unlock()
	return null
