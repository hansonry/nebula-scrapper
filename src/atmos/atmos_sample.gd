extends Resource
class_name AtmosSample

var pressure_kpa : float
var temperature_k : float
var mols : Dictionary

func get_mols(molecule: Molecule) -> float:
	return mols[molecule]
func has_mols(molecule: Molecule) -> bool:
	return mols.has(molecule)
	
func set_mols(molecule: Molecule, amount: float):
	mols[molecule] = amount

func set_from_volume(volume: AtmosVolume):
	pressure_kpa = volume.pressure_kpa
	temperature_k = volume.temperature_k
	for i in range(volume.config.mol_list.size()):
		var molecule: Molecule = volume.config.mol_list[i]
		mols[molecule] = volume.slot.get_value(i)

func get_molecules() -> Array:
	return mols.keys()

func copy(out : AtmosSample = null) -> AtmosSample:
	if out == null:
		out = AtmosSample.new()
	out.clear(true)
	out.pressure_kpa   = pressure_kpa
	out.temperature_k = temperature_k
	for k in mols.keys():
		out.mols[k] = mols[k]
	return out

func clear(hard: bool = false):
	pressure_kpa = 0
	temperature_k = 0
	if hard:
		mols.clear()
	else:
		for key in mols:
			mols[key] = 0

func remove_below(mol_threashold: float = 0.0001):
	for key in mols.keys():
		if mols[key] < mol_threashold:
			mols.erase(key)

func get_total_mols() -> float:
	var sum : float = 0
	for key in mols.keys():
		sum += mols[key]
	return sum
