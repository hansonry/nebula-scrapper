extends Node3D
class_name AtmosNode

var _mutex : Mutex = Mutex.new()

var _thread_sample : AtmosSample = AtmosSample.new()
var _sample :AtmosSample = AtmosSample.new()
var _update: bool = true
var _thread_safe_global_pos: Vector3

@export var sample : AtmosSample:
	get:
		_mutex.lock()
		if _update and _thread_sample != null:
			# TODO: This is probably going to cause issues because we should
			# be at zero pressure here instead of whatever was stored last
			_thread_sample.copy(_sample)
			_update = false
		_mutex.unlock()
		return _sample


# Called when the node enters the scene tree for the first time.
func _ready():
	GameAtmosSystem.atmos_step_after.signal_connect(_protected_atmos_measure)



func _exit_tree():
	GameAtmosSystem.atmos_step_after.signal_disconnect(_protected_atmos_measure)

func _protected_atmos_measure():
	_mutex.lock()
	_atmos_measure()
	_mutex.unlock()

func _atmos_measure():
	var sample :AtmosSample = GameAtmosSystem.get_sample_at(_thread_safe_global_pos,  _thread_sample)
	if sample == null:
		_thread_sample.clear()
	_update = true
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if GameAtmosSystem.paused:
		_mutex.lock()
		_thread_safe_global_pos = global_position
		_mutex.unlock()
		_atmos_measure()
	else:
		_mutex.lock()
		_thread_safe_global_pos = global_position
		_mutex.unlock()
