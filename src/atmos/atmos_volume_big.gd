extends Object
class_name AtmosVolumeBig


var volume: AtmosVolume = null
var link_xp : AtmosLink = null
var link_xn : AtmosLink = null
var link_yp : AtmosLink = null
var link_yn : AtmosLink = null
var link_zp : AtmosLink = null
var link_zn : AtmosLink = null
var big_position : Vector3i
var is_outside : bool = false

func _init(atmos_system: AtmosSystem, _big_position : Vector3i):
	volume = atmos_system.volume_add(8000)
	big_position = _big_position

func get_link_name_by_slot(slot:BigMap.Slot) -> String:
	var result: String
	match slot:
		BigMap.Slot.FRONT:
			result = "link_zn"
		BigMap.Slot.BACK:
			result = "link_zp"
		BigMap.Slot.LEFT:
			result = "link_xp"
		BigMap.Slot.RIGHT:
			result = "link_xn"
		BigMap.Slot.UP:
			result = "link_yn"
		BigMap.Slot.DOWN:
			result = "link_yp"
		_:
			result = ""
	return result
