extends RefCounted
class_name AtmosSlotPtr

var memory: AtmosSlotMemory = null
var index : int

func _init(_memory: AtmosSlotMemory, _index: int):
	memory = _memory
	index = _index

func get_value(value_index: int) -> float:
	return memory.data[index + value_index]

func set_value(value_index: int, mols: float):
	memory.data[index + value_index] = mols

func add_value(value_index: int, mols: float):
	memory.data[index + value_index] += mols

func compute_total() -> float:
	var total: float = 0
	for value_index : int in memory.values_index():
		total += memory.data[index + value_index]
	return total

func compute_const_volume_specific_heat(config:AtmosConfig)-> float:
	var total_specific_heat : float = 0
	for value_index : int in memory.values_index():
		total_specific_heat += memory.data[index + value_index] * config.mol_list[value_index].volume_heat_capacity
	return total_specific_heat

func compute_mass_g(config:AtmosConfig)-> float:
	var mass : float = 0
	for value_index : int in memory.values_index():
		mass += (memory.data[index + value_index] * config.mol_list[value_index].molar_mass_g)
	return mass


func compute_temperature_K(config: AtmosConfig, energy_j:float ) -> float:
	var total_specific_heat: float = compute_const_volume_specific_heat(config)
	return AtmosMath.zdiv(energy_j, total_specific_heat, 0) 

func set_to_percent_of(src: AtmosSlotPtr, percent: float):
	for value_index : int in memory.values_index():
		memory.data[index + value_index] = src.memory.data[src.index + value_index] * percent

func merge(src: AtmosSlotPtr):
	for value_index : int in memory.values_index():
		memory.data[index + value_index] += src.memory.data[src.index + value_index]

func clear():
	for value_index : int in memory.values_index():
		memory.data[index + value_index] = 0

func copy(src: AtmosSlotPtr) -> void:
	for value_index: int in memory.values_index():
		memory.data[index + value_index] = src.memory.data[src.index + value_index]
		
func scale(amount: float) -> void:
	for value_index: int in memory.values_index():
		memory.data[index + value_index] = memory.data[index + value_index] * amount
