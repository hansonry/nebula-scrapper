extends Resource
class_name Molecule

enum Phase {SOLID, LIQUID, GAS}

@export var name : String
@export var heat_capacity_ratio: float:
	set(value):
		heat_capacity_ratio = value
		_update_capacities = true
@export var molar_mass_g: float = 1
@export var phase : Phase = Phase.GAS


var _update_capacities: bool = true

var volume_heat_capacity:
	get:
		if _update_capacities:
			volume_heat_capacity =  AtmosMath.r / (heat_capacity_ratio - 1)
			_update_capacities = false
		return volume_heat_capacity
