extends Resource
class_name AtmosLink

enum Mode {OPEN, CLOSED, CLOSED_ENERGY_XFER, PUMP_VOLUME, PUMP_MOL}

var volume_a : AtmosVolume = null
var volume_b : AtmosVolume = null

var thermal_conductivity_factor : float = 1 # Thermal Conductivity / conducting material width * conducting material area
var area_m2 : float = 0.01
var flow : float = 0
var partial_flows : AtmosSlotPtr = null
var mode_a_to_b: Mode = Mode.OPEN
var mode_b_to_a: Mode = Mode.OPEN
var pump_a_to_b_amount : float = 0
var pump_b_to_a_amount : float = 0


func set_open():
	mode_a_to_b = Mode.OPEN
	mode_b_to_a = Mode.OPEN

func set_closed():
	mode_a_to_b = Mode.CLOSED
	mode_b_to_a = Mode.CLOSED

func set_pump_volume_a_to_b(volume_l: float):
	mode_a_to_b = Mode.PUMP_VOLUME
	mode_b_to_a = Mode.CLOSED
	pump_a_to_b_amount = volume_l
	pump_b_to_a_amount = 0
	
func set_pump_volume_b_to_a(volume_l: float):
	mode_a_to_b = Mode.CLOSED
	mode_b_to_a = Mode.PUMP_VOLUME
	pump_a_to_b_amount = 0
	pump_b_to_a_amount = volume_l

func set_pump_mols_a_to_b(mols: float):
	mode_a_to_b = Mode.PUMP_MOL
	mode_b_to_a = Mode.CLOSED
	pump_a_to_b_amount = mols
	pump_b_to_a_amount = 0
	
func set_pump_mols_b_to_a(mols: float):
	mode_a_to_b = Mode.CLOSED
	mode_b_to_a = Mode.PUMP_MOL
	pump_a_to_b_amount = 0
	pump_b_to_a_amount = mols

func set_energy_only():
	mode_a_to_b = Mode.CLOSED_ENERGY_XFER
	mode_b_to_a = Mode.CLOSED_ENERGY_XFER

func set_one_way_a_to_b():
	mode_a_to_b = Mode.OPEN
	mode_b_to_a = Mode.CLOSED
	
func set_one_way_b_to_a():
	mode_a_to_b = Mode.CLOSED
	mode_b_to_a = Mode.OPEN
