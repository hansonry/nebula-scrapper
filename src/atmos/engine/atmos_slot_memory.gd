extends Resource
class_name AtmosSlotMemory


const _flags_unit_size = 32


var _usedFlags : PackedInt32Array
var data : PackedFloat32Array
var slot_size: int = 0
var _mols_range : Array
var _values_range : Array

func _init(molocule_count: int = 0):
	slot_size = molocule_count
	_values_range = range(slot_size)
	_grow_by()

func values_index() -> Array:
	return _values_range

func _grow_by(chunks: int = 2):
	var old_flags_size : int = _usedFlags.size()
	var new_flags_size : int = old_flags_size + chunks
	_usedFlags.resize(new_flags_size)
	for i in range(old_flags_size, new_flags_size):
		_usedFlags[i] = 0
	data.resize(new_flags_size * _flags_unit_size * slot_size)

func request() -> AtmosSlotPtr:
	for flag_index in range(_usedFlags.size()):
		var flags:int = _usedFlags[flag_index]
		if flags != 0xFFFFFFFF:
			var mask :int = 1
			for bit_index in range(_flags_unit_size):
				if (flags & mask) == 0:
					flags = flags | mask
					_usedFlags[flag_index] = flags
					return AtmosSlotPtr.new(self, 
						(flag_index * _flags_unit_size + bit_index) * slot_size)
				mask = mask << 1
	var flag_index : int = _usedFlags.size()
	_grow_by()
	_usedFlags[flag_index] = 1
	return AtmosSlotPtr.new(self, flag_index * _flags_unit_size * slot_size)

func release(ptr: AtmosSlotPtr):
	@warning_ignore("integer_division")
	var index_sans_molocules : int = ptr.index / slot_size
	@warning_ignore("integer_division")
	var flags_index : int = index_sans_molocules /  _flags_unit_size
	var bit_offset : int = index_sans_molocules % _flags_unit_size
	var mask : int = 1 << bit_offset
	var flags: int = _usedFlags[flags_index]
	flags = flags & ~mask
	_usedFlags[flags_index] = flags
