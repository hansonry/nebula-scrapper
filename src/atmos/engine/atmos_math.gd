extends Object
class_name AtmosMath

const r : float = 8.31446261815324

const _c_to_k : float = 273.15

static func calculate_pressure(volume_l : float, temperature_k : float,  mols: float) -> float:
	return zdiv(mols * r * temperature_k, volume_l, 0)

static func calculate_mols(pressure_kpa : float, volume_l : float, temperature_k : float) -> float:
	return zdiv(pressure_kpa * volume_l, r * temperature_k, 0)

static func celsius_to_kelvin(celsius: float) -> float:
	return celsius + _c_to_k

static func kelvin_to_celsius(kelvin: float) -> float:
	return kelvin - _c_to_k
	
static func zdiv(a: float, b: float, if0: float) -> float:
	if abs(b) < 0.00001:
		return if0
	return a / b
