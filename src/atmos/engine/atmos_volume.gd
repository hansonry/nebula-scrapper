extends Resource
class_name AtmosVolume

var config : AtmosConfig = null
var volume_l : float = 1 :
	set(value):
		dirty_flag = true
		volume_l = value
var slot : AtmosSlotPtr = null
var partial_pressure_kpa : AtmosSlotPtr = null
var total_mols : float :
	get:
		update_if_dirty()
		return total_mols
var temperature_k : float :
	get:
		update_if_dirty()
		return temperature_k
var pressure_kpa : float :
	get:
		update_if_dirty()
		return pressure_kpa
var average_molar_mass_g : float :
	get:
		update_if_dirty()
		return average_molar_mass_g
var energy_j : float
var locked : bool = false # No changes alowed to energy or mols
var dirty_flag : bool = true
var link_count: int = 0
var _archive_pressure_kpa : float = 0
var _archive_temperature_k : float = 0
var _share_coff : float = 0

func update_if_dirty() -> void:
	if dirty_flag:
		var _total_mols: float = slot.compute_total()
		var _temperature_k = slot.compute_temperature_K(config, energy_j)
		var _mass_g = slot.compute_mass_g(config)
		total_mols = _total_mols
		temperature_k = _temperature_k
		pressure_kpa = AtmosMath.calculate_pressure(volume_l, _temperature_k, _total_mols)
		average_molar_mass_g = AtmosMath.zdiv(_mass_g, _total_mols, 0)
		dirty_flag = false

func set_mols(mols: Dictionary, _temperature_k : float) -> void:
	slot.clear()
	for key in mols.keys():
		var mol_index: int = config.index_of_molecule_by_name(key)
		assert(mol_index >= 0, "%s is not a known molecule" % key)
		slot.set_value(mol_index, mols[key])
	var specific_heat : float = slot.compute_const_volume_specific_heat(config)
	energy_j = _temperature_k * specific_heat
	dirty_flag = true

func set_temperature(_temperature_k: float) -> void:
	var specific_heat : float = slot.compute_const_volume_specific_heat(config)
	energy_j = _temperature_k * specific_heat
	dirty_flag = true

func _compute_specific_heat_of_mols(mols:Dictionary) -> float:
	var specific_heat: float = 0
	for key in mols.keys():
		var mol_index: int = config.index_of_molecule_by_name(key)
		assert(mol_index >= 0, "%s is not a known molecule" % key)
		specific_heat += mols[key] * config.mol_list[mol_index].volume_heat_capacity
	return specific_heat


func add_mols(mols:Dictionary, _temperature_k: float) -> void:
	var specific_head_of_added_mols : float = _compute_specific_heat_of_mols(mols)
	var added_energy_j = _temperature_k * specific_head_of_added_mols
	for key in mols.keys():
		var mol_index: int = config.index_of_molecule_by_name(key)
		assert(mol_index >= 0, "%s is not a known molecule" % key)
		slot.add_value(mol_index, mols[key])
	energy_j += added_energy_j
	dirty_flag = true

func clear() -> void:
	slot.clear()
	energy_j = 0
	dirty_flag = true

func step_make_archive() -> void:
	_archive_pressure_kpa = pressure_kpa
	_archive_temperature_k = temperature_k
	_share_coff = 1.0 / (link_count + 1)
	var local_total_mols : float = total_mols
	for mol_index: int in slot.memory.values_index():
		var percent = AtmosMath.zdiv(slot.get_value(mol_index), local_total_mols, 0)
		partial_pressure_kpa.set_value(mol_index, percent * _archive_pressure_kpa)


func step_share_gas(link: AtmosLink, volume_b : AtmosVolume) -> float:
	var flow : float = 0
	for mol_index: int in slot.memory.values_index():
		var a_partial_pressure: float = partial_pressure_kpa.get_value(mol_index)
		var b_partial_pressure: float = volume_b.partial_pressure_kpa.get_value(mol_index)
		var partial_pressure_diff : float = b_partial_pressure - a_partial_pressure
		var xfer : float = 0
		var source_temperature_k : float = 0
		if partial_pressure_diff > 0:
			var want : float = AtmosMath.calculate_mols(partial_pressure_diff, volume_l, volume_b._archive_temperature_k)
			xfer = want * volume_b._share_coff
			xfer = min(xfer, volume_b.slot.get_value(mol_index))
			source_temperature_k = volume_b._archive_temperature_k
		else:
			var want : float = AtmosMath.calculate_mols(partial_pressure_diff, volume_b.volume_l, _archive_temperature_k)
			xfer = want * _share_coff
			xfer = max(xfer, -slot.get_value(mol_index))
			source_temperature_k = _archive_temperature_k
		
		var heat_capacity : float = xfer * config.mol_list[mol_index].volume_heat_capacity
		var engery_j : float = source_temperature_k * heat_capacity
		
		if not locked:
			slot.add_value(mol_index, xfer)
			energy_j += engery_j
		if not volume_b.locked:
			volume_b.slot.add_value(mol_index, -xfer)
			volume_b.energy_j -= engery_j
		if not (locked and volume_b.locked):
			flow += xfer
			link.partial_flows.set_value(mol_index, xfer)
	dirty_flag = true
	volume_b.dirty_flag = true
	return flow
