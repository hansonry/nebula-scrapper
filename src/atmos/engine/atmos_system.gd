extends Resource
class_name AtmosSystem

var _config : AtmosConfig
var _volumes : Array[AtmosVolume]
var _links : Array[AtmosLink]
var _slot_memory : AtmosSlotMemory = null
var _link_list : Array[int] = []
var _shuffle_index : int = 0
const _shuffle_set : Array[int] = [
	0, 2, 
	0, 3,
	1, 2,
	1, 3,
]

func _init(config : AtmosConfig = null):
	_config = config
	_slot_memory = AtmosSlotMemory.new(config.mol_list_size())


func volume_add(volume_l: float) -> AtmosVolume:
	var volume: AtmosVolume = AtmosVolume.new()
	volume.config = _config
	volume.volume_l = volume_l
	volume.slot = _slot_memory.request()
	volume.partial_pressure_kpa = _slot_memory.request()
	_volumes.push_back(volume)
	return volume
	
func volume_remove(volume: AtmosVolume) -> void:
	var index: int = _volumes.find(volume)
	if index >= 0:
		
		# Remove links
		for link : AtmosLink in _links:
			if link.volume_a == volume or link.volume_b == volume:
				link_remove(link)

		_volumes.remove_at(index)
		_slot_memory.release(volume.slot)
		_slot_memory.release(volume.partial_pressure_kpa)
		
	
func link_add(volume_a : AtmosVolume, volume_b : AtmosVolume) -> AtmosLink:
	var link: AtmosLink = AtmosLink.new()
	link.volume_a = volume_a
	link.volume_b = volume_b
	link.volume_a.link_count += 1
	link.volume_b.link_count += 1
	link.partial_flows = _slot_memory.request()
	_links.push_back(link)
	_link_list.push_back(_links.size() - 1)
	return link

func link_remove(link: AtmosLink) -> void:
	var index: int = _links.find(link)
	if index >= 0:
		_links.remove_at(index)
		var index_list_index : int = _link_list.find(_links.size())
		if index_list_index >= 0:
			_link_list.remove_at(index_list_index)
		link.volume_a.link_count -= 1
		link.volume_b.link_count -= 1
		_slot_memory.release(link.partial_flows)

func _swap_list_elements(index1: int, index2: int) -> void:
	var temp: int = _link_list[index1]
	_link_list[index1] = _link_list[index2]
	_link_list[index2] = temp

func _shuffle_link_list(start: int, jump: int) -> void:
	var a: int = start
	var b: int = a + jump
	while b < _link_list.size():
		_swap_list_elements(a, b)
		a = b
		b += jump

func _step_shuffle_link_list() -> void:
	_shuffle_link_list(_shuffle_set[_shuffle_index], _shuffle_set[_shuffle_index + 1])
	_shuffle_index += 2
	if _shuffle_index >= _shuffle_set.size():
		_shuffle_index = 0

func _step_make_archive() -> void:
	for volume : AtmosVolume in _volumes:
		volume.step_make_archive()


func _step_step_share_gas() -> void:
	for index : int in _link_list:
		var link : AtmosLink = _links[index]
		link.flow = link.volume_a.step_share_gas(link, link.volume_b)



func step() -> void:
	_step_shuffle_link_list()
	_step_make_archive()
	_step_step_share_gas()
