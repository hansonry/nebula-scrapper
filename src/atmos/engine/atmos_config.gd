extends Resource
class_name AtmosConfig

@export var mol_list : Array[Molecule] = []


func mol_list_size():
	return mol_list.size()

func get_molecule(index: int) -> Molecule:
	if index >= 0 and index < mol_list.size():
		return mol_list[index]
	return null

func index_of_molecule(mol :Molecule) -> int:
	return mol_list.find(mol)

func index_of_molecule_by_name(mol_name: String) -> int:
	for mol_index in range(mol_list.size()):
		if mol_list[mol_index].name == mol_name:
			return mol_index
	return -1
