extends Control

@onready var container :TabContainer = $TabContainer

@onready var simulation : PackedScene = preload("res://atmos/engine/test/atmos_test_simulation.tscn")

@export var solo_index: int = -1
@export var test_list: Array[Script]


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	if solo_index < 0:
		for script :Script in test_list:
			var scene = simulation.instantiate()
			scene.set_script(script)
			container.add_child(scene)
	else:
			var scene = simulation.instantiate()
			scene.set_script(test_list[solo_index])
			container.add_child(scene)



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass
