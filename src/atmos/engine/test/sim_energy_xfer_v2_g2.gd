extends AtmosTestSimulation


func _sim_setup():
	name = "Energy Xfer V2 G2"
	_target_steps = 50
	
	var config :AtmosConfig = AtmosConfig.new()
	config.mol_list.push_back(preload("res://atmos/molecule_oxygen.tres"))
	config.mol_list.push_back(preload("res://atmos/molecule_volatile.tres"))
	_system = AtmosSystem.new(config)
	
	var volume_1: AtmosVolume = volume_add(1)
	var volume_2: AtmosVolume = volume_add(1)
	var link : AtmosLink = link_add(volume_1, volume_2)
	link.set_energy_only()
	link.thermal_conductivity_factor = 0.5
	
	volume_1.set_mols({"Oxygen": 200}, 273.15)
	volume_2.set_mols({"Volatile": 200}, 373.15)
	
	
