extends AtmosTestSimulation


func _sim_setup():
	name = "One Way V2 G1 Mirrored"
	_target_steps = 50
	
	var config :AtmosConfig = AtmosConfig.new()
	config.mol_list.push_back(preload("res://atmos/molecule_oxygen.tres"))
	_system = AtmosSystem.new(config)
	
	var volume_1: AtmosVolume = volume_add(1)
	var volume_2: AtmosVolume = volume_add(1)
	var volume_3: AtmosVolume = volume_add(1)
	var volume_4: AtmosVolume = volume_add(1)

	var link_1 : AtmosLink = link_add(volume_1, volume_2)
	var link_2 : AtmosLink = link_add(volume_3, volume_4)
	
	link_1.set_closed()
	link_2.set_closed()
	
	volume_1.set_mols({"Oxygen": 200}, 294)
	volume_4.set_mols({"Oxygen": 200}, 294)
	
	add_step_callback(5, func():
		link_1.set_one_way_a_to_b()
		link_2.set_one_way_b_to_a()
	)
	
	
