extends AtmosTestSimulation


func _sim_setup():
	name = "Pump Mol V2 G1 Mirrored"
	_target_steps = 50
	
	var config :AtmosConfig = AtmosConfig.new()
	config.mol_list.push_back(preload("res://atmos/molecule_oxygen.tres"))
	_system = AtmosSystem.new(config)
	
	var volume_1: AtmosVolume = volume_add(1)
	var volume_2: AtmosVolume = volume_add(1)
	var volume_3: AtmosVolume = volume_add(1)
	var volume_4: AtmosVolume = volume_add(1)

	var link_1 : AtmosLink = link_add(volume_1, volume_2)
	var link_2 : AtmosLink = link_add(volume_3, volume_4)
	
	link_1.set_pump_mols_a_to_b(10)
	link_2.set_pump_mols_b_to_a(10)
	
	volume_1.set_mols({"Oxygen": 200}, 294)
	volume_2.set_mols({"Oxygen": 200}, 294)
	volume_3.set_mols({"Oxygen": 200}, 294)
	volume_4.set_mols({"Oxygen": 200}, 294)
	
	
	
