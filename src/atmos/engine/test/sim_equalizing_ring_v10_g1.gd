extends AtmosTestSimulation


func _sim_setup():
	name = "Equalizing Ring V10 G1"
	var target_volumes: int = 10
	_target_steps = 100
	
	var config :AtmosConfig = AtmosConfig.new()
	config.mol_list.push_back(preload("res://atmos/molecule_oxygen.tres"))
	_system = AtmosSystem.new(config)
	
	var volumes: Array[AtmosVolume] = []
	volumes.resize(target_volumes)
	for index in range(target_volumes):
		volumes[index] = volume_add(1)
	
	for index in range(target_volumes - 1):
		link_add(volumes[index], volumes[index + 1])
	
	link_add(volumes[target_volumes -1], volumes[0])
	
	volumes[0].set_mols({"Oxygen": 200}, 294)
	
	
