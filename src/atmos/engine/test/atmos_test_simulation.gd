extends VBoxContainer
class_name AtmosTestSimulation

@onready var chart_pressure: Chart = $Pressure
@onready var chart_temperature: Chart = $Temperature

const packed_chart = preload("res://addons/easy_charts/control_charts/chart.tscn")

var _system : AtmosSystem = null
var _target_steps : int = 0
var _volumes : Array[AtmosVolume]
var _volume_names : Array[String]
var _xs: Array[float] = []
var _pressures: Array = []
var _temperatures: Array = []
var _mols: Array = []
var _simulation_name : String = ""
var _mol_charts: Array[Chart]
var _step_callbacks: Array[Dictionary]

const colors = ['#6F4C9B', '#6059A9', '#5568B8', '#4E79C5', '#4D8AC6',
				'#4E96BC', '#549EB3', '#59A5A9', '#60AB9E', '#69B190',
				'#77B77D', '#8CBC68', '#A6BE54', '#BEBC48', '#D1B541',
				'#DDAA3C', '#E49C39', '#E78C35', '#E67932', '#E4632D',
				'#DF4828', '#DA2222', '#B8221E', '#95211B', '#721E17',
				'#521A13']

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	_sim_setup()
	assert(_system != null, "Expected _system to be populated")
	assert(_target_steps > 0, "Expected _target_steps to be larger than zero")
	
	
	
	_run_simulation()
	
	_setup_pressure_and_temperature_charts()
	_create_mols_charts()

func _sort_step_callbacks():
	_step_callbacks.sort_custom(func(a, b): return a.step < b.step)

func _get_mol_range():
	return range(_system._config.mol_list.size())

func _run_simulation():
	
	# Initalize Storage
	var volume_range = range(_volumes.size())
	var mol_range = _get_mol_range()
	_xs.resize(_target_steps)
	_pressures.resize(_volumes.size())
	_temperatures.resize(_volumes.size())
	for index in volume_range:
		_pressures[index] = []
		_pressures[index].resize(_target_steps)
		_temperatures[index] = []
		_temperatures[index].resize(_target_steps)
	_mols.resize(mol_range.size())
	for mol_index in mol_range:
		_mols[mol_index] = []
		_mols[mol_index].resize(_volumes.size())
		for volume_index in volume_range:
			_mols[mol_index][volume_index] = []
			_mols[mol_index][volume_index].resize(_target_steps)
	
	
	# Run simulation
	var callback_index : int = 0
	for step: int in range(_target_steps):
		while callback_index < _step_callbacks.size() and _step_callbacks[callback_index].step == step:
			_step_callbacks[callback_index].callback.call()
			callback_index += 1
		_xs[step] = step
		for volume_index in volume_range:
			var volume: AtmosVolume = _volumes[volume_index]
			_pressures[volume_index][step] = volume.pressure_kpa
			_temperatures[volume_index][step] = volume.temperature_k
			for mol_index in mol_range:
				_mols[mol_index][volume_index][step] = volume.slot.get_value(mol_index)
		_system.step()

func _get_color_by_index(index: int) -> Color:
	var circular_index: int = index % colors.size()
	return Color(colors[circular_index])

func _setup_pressure_and_temperature_charts():
	var volume_range = range(_volumes.size())
	var pressure_cp: ChartProperties = _create_chart_properties()
	pressure_cp.y_label = "Pressure KPa"
	pressure_cp.max_samples = _target_steps
	var pressure_funcs : Array[Function]
	pressure_funcs.resize(_volumes.size())
	for volume_index in volume_range:
		pressure_funcs[volume_index] = Function.new(
			_xs, _pressures[volume_index], _volume_names[volume_index], {
				color = _get_color_by_index(volume_index),
				marker = Function.Marker.CIRCLE,
				type = Function.Type.LINE,
				interpolation = Function.Interpolation.LINEAR
			}
		)
	chart_pressure.plot(pressure_funcs, pressure_cp)
	
	var temperature_cp: ChartProperties = _create_chart_properties()
	temperature_cp.y_label = "Temperature K"
	temperature_cp.max_samples = _target_steps
	var temperature_funcs : Array[Function]
	temperature_funcs.resize(_volumes.size())
	for volume_index in volume_range:
		temperature_funcs[volume_index] = Function.new(
			_xs, _temperatures[volume_index], _volume_names[volume_index], {
				color = _get_color_by_index(volume_index),
				marker = Function.Marker.CIRCLE,
				type = Function.Type.LINE,
				interpolation = Function.Interpolation.LINEAR
			}
		)
	chart_temperature.plot(temperature_funcs, temperature_cp)

	
func _create_mols_charts():
	var volume_range = range(_volumes.size())
	var mol_range = _get_mol_range()
	for mol_index in mol_range:
		var chart : Chart = packed_chart.instantiate()
		chart.ready.connect(func():
			var cp: ChartProperties = _create_chart_properties()
			cp.y_label = _system._config.mol_list[mol_index].name
			cp.max_samples = _target_steps
			var funcs : Array[Function]
			funcs.resize(_volumes.size())
			for volume_index in volume_range:
				funcs[volume_index] = Function.new(
					_xs, _mols[mol_index][volume_index], _volume_names[volume_index], {
						color = _get_color_by_index(volume_index),
						marker = Function.Marker.CIRCLE,
						type = Function.Type.LINE,
						interpolation = Function.Interpolation.LINEAR
					}
				)
			chart.plot(funcs, cp)
			_mol_charts.push_back(chart)
			)
		add_child(chart)


func _sim_setup():
	assert(false, "Expected method to be overridden")

func volume_add(volume_l: float, name: String = "") -> AtmosVolume:
	assert(_system != null, "Expected _system to be populated")
	var volume : AtmosVolume = _system.volume_add(volume_l)
	if name == "":
		name = "v%d" % _volumes.size()
	_volumes.push_back(volume)
	_volume_names.push_back(name)
	return volume

func link_add(volume_a: AtmosVolume, volume_b: AtmosVolume) -> AtmosLink:
	assert(_system != null, "Expected _system to be populated")
	var link : AtmosLink = _system.link_add(volume_a, volume_b)
	return link


func _create_chart_properties() -> ChartProperties:
		# Let's customize the chart properties, which specify how the chart
	# should look, plus some additional elements like labels, the scale, etc...
	var cp: ChartProperties = ChartProperties.new()
	cp.colors.frame = Color("#161a1d")
	cp.colors.background = Color.TRANSPARENT
	cp.colors.grid = Color("#283442")
	cp.colors.ticks = Color("#283442")
	cp.colors.text = Color.WHITE_SMOKE
	cp.draw_bounding_box = false
	cp.show_legend = true
	cp.title = ""
	cp.x_label = "Steps"
	cp.y_label = ""
	cp.x_scale = 10
	cp.y_scale = 5
	cp.interactive = true # false by default, it allows the chart to create a tooltip to show point values
	# and interecept clicks on the plot
	return cp

func add_step_callback(step: int, callback: Callable):
	_step_callbacks.push_back({
		"step": step,
		"callback": callback
	})

func redraw_plots():
	#for chart: Chart in _mol_charts:
	#	chart.load_functions(chart.functions)
	#chart_pressure.load_functions(chart_pressure.functions)
	#chart_temperature.load_functions(chart_temperature.functions)
	pass
