extends AtmosTestSimulation


func _sim_setup():
	name = "Equalizing Rect V6 G1"
	_target_steps = 50
	
	var config :AtmosConfig = AtmosConfig.new()
	config.mol_list.push_back(preload("res://atmos/molecule_oxygen.tres"))
	_system = AtmosSystem.new(config)
	
	var volume_1: AtmosVolume = volume_add(1)
	var volume_2: AtmosVolume = volume_add(1)
	var volume_3: AtmosVolume = volume_add(1)
	var volume_4: AtmosVolume = volume_add(1)
	var volume_5: AtmosVolume = volume_add(1)
	var volume_6: AtmosVolume = volume_add(1)
	var link_1 : AtmosLink = link_add(volume_1, volume_2)
	var link_2 : AtmosLink = link_add(volume_2, volume_3)
	
	var link_3 : AtmosLink = link_add(volume_4, volume_5)
	var link_4 : AtmosLink = link_add(volume_5, volume_6)
	
	var link_5 : AtmosLink = link_add(volume_1, volume_4)
	var link_6 : AtmosLink = link_add(volume_2, volume_5)
	var link_7 : AtmosLink = link_add(volume_3, volume_6)
	
	volume_1.set_mols({"Oxygen": 200}, 294)
	
	
