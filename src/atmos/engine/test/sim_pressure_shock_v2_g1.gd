extends AtmosTestSimulation


func _sim_setup():
	name = "Pressure Shock V2 G1"
	_target_steps = 30
	
	var config :AtmosConfig = AtmosConfig.new()
	config.mol_list.push_back(preload("res://atmos/molecule_oxygen.tres"))
	_system = AtmosSystem.new(config)
	
	var volume_1: AtmosVolume = volume_add(1)
	var volume_2: AtmosVolume = volume_add(1)
	var link : AtmosLink = link_add(volume_1, volume_2)
	
	volume_1.set_mols({"Oxygen": 200}, 294)
	volume_1.locked = true
	
	add_step_callback(15, func():
		volume_1.clear()
	)
	
	
	
