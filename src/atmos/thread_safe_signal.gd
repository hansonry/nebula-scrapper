extends RefCounted
class_name ThreadSafeSignal

var list: Array[Callable]
var mutex : Mutex

func _init():
	mutex = Mutex.new()

func signal_connect(callable: Callable):
	mutex.lock()
	list.append(callable)
	mutex.unlock()
func signal_disconnect(callable: Callable):
	mutex.lock()
	list.remove_at(list.find(callable))
	mutex.unlock()
	
func signal_emit():
	mutex.lock()
	for c : Callable in list:
		c.call()
	mutex.unlock()
