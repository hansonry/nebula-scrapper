extends Object
class_name AtmosPipes


const PIPE_VOLUME: float = 10

var network_manager: ExternalConnectorNetworkManager = null:
	set(value):
		if network_manager != null:
			network_manager.network_added.disconnect(_network_manager_network_added)
			network_manager.network_removed.disconnect(_network_manager_network_removed)
			network_manager.network_post_split.disconnect(_network_manager_post_split)
		network_manager = value
		if network_manager != null:
			network_manager.network_added.connect(_network_manager_network_added)
			network_manager.network_removed.connect(_network_manager_network_removed)
			network_manager.network_post_split.connect(_network_manager_post_split)


var _data_lookup: Dictionary = {}

func _init(_network_manager: ExternalConnectorNetworkManager):
	network_manager = _network_manager
	
func _network_manager_network_added(network: ExternalConnectorNetwork):
	var data : Dictionary = {
		&"network": network,
		&"volume": null,
	}
	_data_lookup[network] = data
	GameAtmosSystem.queue_callable(func(atmos_system: AtmosSystem):
		data[&"volume"] = atmos_system.volume_add(0)
	)
	network.node_added.connect(_network_node_added.bind(data))
	network.node_removed.connect(_network_node_remvoed.bind(data))

func _network_manager_network_removed(network: ExternalConnectorNetwork):
	var data : Dictionary = _data_lookup[network]
	GameAtmosSystem.queue_callable(func(atmos_system: AtmosSystem):
		atmos_system.volume_remove(data[&"volume"])
	)
	data.erase(network)

func _network_manager_post_split(seed_network: ExternalConnectorNetwork, new_networks: Array[ExternalConnectorNetwork]):
	var split_count : int = new_networks.size() + 1
	var seed_network_volume : AtmosVolume = _data_lookup[seed_network][&"volume"]
	var new_network_volumes : Array[AtmosVolume] = []
	new_network_volumes.resize(new_networks.size())
	for index: int in range(new_networks.size()):
		new_network_volumes[index] = _data_lookup[new_networks[index]][&"volume"]
	GameAtmosSystem.queue_callable(func(atmos_system: AtmosSystem):
		var config : AtmosConfig = seed_network_volume.config
		var total_volume: float = seed_network_volume.volume_l
		for volume: AtmosVolume in new_network_volumes:
			total_volume += volume.volume_l
		for volume: AtmosVolume in new_network_volumes:
			var percent : float = volume.volume_l / total_volume
			for mol_index: int in range(config.mol_list.size()):
				volume.slot.set_mol(mol_index, seed_network_volume.slot.get_mol(mol_index) * percent)
				volume.dirty_flag = true
		var seed_percent : float = seed_network_volume.volume_l / total_volume
		for mol_index: int in range(config.mol_list.size()):
			seed_network_volume.slot.set_mol(mol_index, seed_network_volume.slot.get_mol(mol_index) * seed_percent)
	)

func _network_node_added(node: BuildableNode, data: Dictionary):
	GameAtmosSystem.queue_callable(func(atmos_system: AtmosSystem):
		data[&"volume"].volume_l += PIPE_VOLUME
	)

func _network_node_remvoed(node: BuildableNode, data: Dictionary):
	GameAtmosSystem.queue_callable(func(atmos_system: AtmosSystem):
		data[&"volume"].volume_l -= PIPE_VOLUME
	)

func get_volume_from_network(network: ExternalConnectorNetwork) -> AtmosVolume:
	if _data_lookup.has(network):
		var data : Dictionary = _data_lookup[network]
		return data[&"volume"]
	return null
