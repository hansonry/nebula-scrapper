extends Resource
class_name GridConfig3

@export var width: int :
	set(value):
		width = value
		_recompute()
		
@export var height: int:
	set(value):
		height = value
		_recompute()
@export var depth: int:
	set(value):
		depth = value
		_recompute()

@export var cell_size : int:
	set(value):
		cell_size = value
		_recompute()

var _size : int
var _deltas : Vector3i


func _recompute():
	_deltas.x = cell_size
	_deltas.y = _deltas.x * width
	_deltas.z = _deltas.y * height
	_size = _deltas.z * depth

func _init(the_width: int = 1, the_height: int = 1, the_depth: int = 1, the_cell_size: int = 1):
	width = the_width
	height = the_height
	depth = the_depth
	cell_size = the_cell_size
	_recompute()

func size() -> int:
	return _size
	
func deltas() -> Vector3i:
	return _deltas

func is_valid_pos(pos: Vector3i) -> bool:
	return (pos.x >= 0 and pos.y >= 0 and pos.z >= 0 and 
			pos.x < width and pos.y < height and pos.z < depth)

func get_index(pos: Vector3i ) -> int:
	if is_valid_pos(pos):
		return pos.x * _deltas.x + pos.y * _deltas.y + pos.z * _deltas.z
	return -1

func is_valid_index(index: int) -> bool:
	return index >= 0 and index <= _size

func get_position(index: int) -> Vector3i:
	if is_valid_index(index):
		var pos : Vector3i = Vector3i.ZERO
		@warning_ignore("integer_division")
		pos.z = index / _deltas.z 
		index -= pos.z * _deltas.z
		@warning_ignore("integer_division")
		pos.y = index / _deltas.y 
		index -= pos.y * _deltas.y
		@warning_ignore("integer_division")
		pos.x = index / _deltas.x 
		return pos
	return Vector3i(-1, -1, -1)
