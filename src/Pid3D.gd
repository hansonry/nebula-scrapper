extends RefCounted
class_name Pid3D

@export var p : float = 1
@export var i : float = 0
@export var d : float = 0

@export var max_integral_magnatude: float = -1

var _integration : Vector3 = Vector3.ZERO
var _previous_error :Vector3 = Vector3.ZERO

func _init(p_val : float = 1, i_val : float = 0, d_val : float = 0, max_integral_magnatude_val: float = -1):
	p = p_val
	i = i_val
	d = d_val
	max_integral_magnatude = max_integral_magnatude_val

func reset():
	_integration = Vector3.ZERO
	_previous_error = Vector3.ZERO

func reset_x():
	_integration.x = 0
	_previous_error.x = 0

func reset_y():
	_integration.y = 0
	_previous_error.y = 0

func reset_z():
	_integration.z = 0
	_previous_error.z = 0


func update_error(error: Vector3, delta: float) -> Vector3:
	#print(error)
	# Calculate D
	var error_diff_unscaled = (error - _previous_error)
	var error_diff = error_diff_unscaled * delta
	_previous_error = error
	
	
	# Calcualate I
	var prev_integration = _integration
	
	_integration += error * delta
	
	if max_integral_magnatude > 0:
		_integration = _integration.limit_length(max_integral_magnatude)
	
	var command = p * error + i * _integration + d * error_diff
	
	if command.length_squared() > 1:
		_integration = prev_integration
	#print(_integration, error, error_diff, command)
	return command

func update_target(current: Vector3, target : Vector3, delta: float) -> Vector3:
	var error = target - current
	return update_error(error, delta)
