@tool
extends Area3D
class_name ItemSpawner


@export var item_stack : ItemStack = null:
	set(value):
		if Engine.is_editor_hint():
			if value != item_stack:
				if item_stack != null:
					item_stack.changed.disconnect(_on_item_stack_change)
				if value != null:
					value.changed.connect(_on_item_stack_change)
		item_stack = value
		if Engine.is_editor_hint():
			_update_item_type()


@export var linear_velocity : Vector3 = Vector3.ZERO

@export var min_time_between_spawn : float = 1

@export var number_to_spawn: int = 1

@export var detect_item_despawn : bool = false

@export var delete_when_done : bool = true

var spawn_count : int = 0


var _item_type : ItemType = null
var _item_instance = null

var _time_since_last_spawn : float = 0


func _on_item_stack_change():
	_update_item_type()



func _update_item_type():
	var to_item_type : ItemType = null
	if item_stack != null:
		to_item_type = item_stack.item_type
	
	if _item_instance != null:
		remove_child(_item_instance)
		_item_instance.queue_free()
		_item_instance = null
	if to_item_type != null:
		_item_instance = to_item_type.view_scene.instantiate()
		add_child(_item_instance)
		_item_instance.set_owner(self)
	_item_type = to_item_type
		

# Called when the node enters the scene tree for the first time.
func _ready():
	_time_since_last_spawn = min_time_between_spawn


func _spawn(stack: ItemStack):
	var new_item : ItemNode = stack.create_item_node()
	new_item.linear_velocity = linear_velocity
	get_parent_node_3d().add_child(new_item)
	if detect_item_despawn:
		new_item.tree_exited.connect(_spawn_exiting)
	new_item.position = position
	new_item.rotation = rotation
	
func _spawn_exiting():
	if spawn_count > 0:
		spawn_count -= 1
	
func _is_area_clear() -> bool:
	return not (has_overlapping_areas() or has_overlapping_bodies())

func _game_process(delta):
	if _time_since_last_spawn > min_time_between_spawn:
		if _is_area_clear() and (number_to_spawn < 0 or spawn_count < number_to_spawn):
			var cloned_item_stack = item_stack.duplicate()
			_spawn(cloned_item_stack)
			spawn_count += 1
			_time_since_last_spawn = 0
			if number_to_spawn >= 0 and spawn_count >= number_to_spawn and delete_when_done:
				queue_free()
	else:
		_time_since_last_spawn += delta

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not Engine.is_editor_hint():
		_game_process(delta)
