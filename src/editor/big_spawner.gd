@tool
extends Node3D

@export var buildable :Buildable = null:
	set(value):
		buildable = value
		if buildable != null:
			stage = buildable.step_list.size() - 1
		_update_preview()

@export var big_slot: BigMap.Slot:
	set(value):
		big_slot = value
		_update_preview_transform()
		
@export var big_position: Vector3i:
	set(value):
		_big_position = value
		position = big_position * BigMap.GRID_SIZE
		_update_preview_transform()
	get:
		return _big_position

@export var stage : int = 0:
	set(value):
		var l_max: int = 0
		if buildable != null:
			l_max = buildable.step_list.size() - 1
		value = clamp(value, 0, l_max)
		materials.resize(value + 1)
		_update_preview()
	get:
		return materials.size() - 1

@export var materials: Array[ItemType]


var _vessel : Vessel = null
var _prev_stage : int = 0
var _prev_buildable : Buildable = null

var _big_position : Vector3i = Vector3i(0, 0, 0)

var _preview : Node3D = null
var _buildable_node: BigBuildableNode = null


func _update_preview():
	if _prev_buildable != buildable or _prev_stage != stage:
		if _preview != null:
			_preview.queue_free()
			_preview = null
		if buildable != null:
			_preview = buildable.step_list[stage].scene.instantiate(PackedScene.GEN_EDIT_STATE_DISABLED)
			add_child(_preview)
			_preview.owner = self
			_update_preview_transform()
		_prev_buildable = buildable
		_prev_stage = stage
	
	pass

func _make_stack_for_step(step: int) -> ItemStack:
	var build_step : BuildStep = buildable.step_list[step]
	var item_stack: ItemStack = ItemStack.new()
	item_stack.item_type = materials[step]
	item_stack.count = build_step.needed_material_amount
	return item_stack
	

func _spawn_buildable_node():
	_buildable_node = BigBuildableNode.new()
	_buildable_node.buildable = buildable
	_buildable_node.set_inital_items(_make_stack_for_step(0))
	_buildable_node.slot = big_slot
	_buildable_node.big_position = _big_position
	_vessel.add_child(_buildable_node)
	
	_buildable_node.transform = BigMap.make_transform(_big_position, big_slot)
	
	for i  in range(1, materials.size()):
		_buildable_node.assemble(_make_stack_for_step(i))



# Called when the node enters the scene tree for the first time.
func _ready():
	if not Engine.is_editor_hint():
		if _preview != null:
			_preview.queue_free()
		_vessel = Vessel.find(self)
		assert(_vessel != null)
		_vessel.ready.connect(_vessel_ready)

func _vessel_ready():
	_spawn_buildable_node()
	queue_free()


func _big_position_from_position() -> Vector3i:
	var l_big_position : Vector3i = BigMap.big_position_from_local(position)
	return l_big_position

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Engine.is_editor_hint():
		if _preview != null:
			var new_big_position :Vector3i = _big_position_from_position()
			if new_big_position != big_position:
				_big_position = new_big_position
			_update_preview_transform()


func _update_preview_transform():
	if _preview != null:
		_preview.transform = BigMap.make_transform(big_position, big_slot)
		_preview.position -= position
