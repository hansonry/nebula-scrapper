extends Control
class_name Console

@onready var _container : Container = $VBoxContainer
@onready var _previous_commands: TextEdit = $VBoxContainer/PreviousCommands
@onready var _prompt: LineEdit = $VBoxContainer/Prompt

var _history: Array[String] = []

var _commands = []
var _max_command_length: int = 0

@export var open: bool = false:
	set = _set_open



# Called when the node enters the scene tree for the first time.
func _ready():
	
	add_command("help", _help_callback, "Lists all the commands")
	_set_open(open)
	pass # Replace with function body.

func _set_open(value: bool):
	open = value
	_container.visible = value
	if value:
		_container.custom_minimum_size.y = size.y / 3
		_prompt.text = ""
		_prompt.grab_focus()
		_set_scroll_to_max()

func _set_scroll_to_max():
	_previous_commands.scroll_vertical = _previous_commands.get_v_scroll_bar().max_value
	
func write(text:String):
	_previous_commands.text = _previous_commands.text + text + "\n"
	_set_scroll_to_max()
	

func _add_to_console(text:String):
	write("> " + text)

func _help_callback(args):
	write("List of all commands:")
	for command in _commands:
		var padding: int =  _max_command_length - command.command.length()
		write("  %s: %*s%s" % [command.command, padding, "", command.info])
	write("")
func add_command(cmd: String, callback: Callable, info : String, params = null):
	if cmd.length() > _max_command_length:
		_max_command_length = cmd.length()
	var command = {
		command = cmd,
		info = info,
		callback = callback,
		params = params
	}
	_commands.append(command)

func _find_command_index(cmd: String) -> int:
	for index: int in _commands.size():
		if _commands[index].command == cmd:
			return index
	return -1


func remove_command(cmd: String):
	var found_index: int = _find_command_index(cmd)
	if found_index >= 0:
		_commands.remove_at(found_index)

func _process_command(text: String):
	var clean_text: String = text.strip_edges()
	if clean_text != "":
		#print("text: ", text)
		_history.append(clean_text)
		_add_to_console(clean_text)

		var args = clean_text.split(" ", false)
		var command_index:int = _find_command_index(args[0])
		if command_index >= 0:
			_call_callback.call_deferred(_commands[command_index].callback, args)
		else:
			write("Unknown command: " + args[0])
	

func _call_callback(callback, args):
	callback.call(args)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ConsoleSubmit") and _container.visible:
		_process_command(_prompt.text)
		_prompt.text = ""
	if Input.is_action_just_pressed("ConsoleToggle"):
		open = not open
