extends PanelContainer
class_name AtmosInfo



@onready var _label_o2 : Label = $VBoxContainer/HBoxContainerO2/ValueO2
@onready var _label_n  : Label = $VBoxContainer/HBoxContainerN/ValueN
@onready var _label_vol : Label =  $VBoxContainer/HBoxContainerVol/ValueVol
@onready var _label_temp : Label = $VBoxContainer/HBoxContainerTemp/Value
@onready var _label_pressure: Label = $VBoxContainer/HBoxContainerPressure/Value

func _apply_atmos_ui_label(label :Label, mol: Molecule, sample: AtmosSample):
	if sample == null or not sample.has_mols(mol):
		label.text = "0"
	else:
		var mols :float = sample.get_mols(mol)
		label.text = "%0.02f" % mols




func set_sample(sample: AtmosSample):
	var mol_oxygen: Molecule = preload("res://atmos/molecule_oxygen.tres")
	var mol_nitrogen: Molecule = preload("res://atmos/molecule_nitrogen.tres")
	var mol_volatile: Molecule = preload("res://atmos/molecule_volatile.tres")
	_apply_atmos_ui_label(_label_o2, mol_oxygen, sample)
	_apply_atmos_ui_label(_label_n, mol_nitrogen, sample)
	_apply_atmos_ui_label(_label_vol, mol_volatile, sample)
	if sample == null:
		_label_temp.text = "nil"
		_label_pressure.text = "nil"
	else:
		_label_temp.text = "%0.02f" % sample.temperature_k
		_label_pressure.text = "%0.02f" % sample.pressure_kpa

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
