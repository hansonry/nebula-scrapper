extends BoxContainer
class_name Hands


@onready var _hand_left = $HandLeft
@onready var _hand_right = $HandRight

var _character : Character = null

func set_character(character : Character):
	var item_slot_ui_left :ItemSlotUI = _hand_left.get_item_slot_ui()
	var item_slot_ui_right :ItemSlotUI = _hand_right.get_item_slot_ui()

	if _character != null:
		item_slot_ui_left.clear_inventory()
		item_slot_ui_right.clear_inventory()
		_character.hand_switched.disconnect(_hand_switched)
	_character = character
	if _character != null:
		_character.hand_switched.connect(_hand_switched)
		item_slot_ui_left.set_inventory(_character.inv_hands, Character.Hand.LEFT)
		item_slot_ui_right.set_inventory(_character.inv_hands, Character.Hand.RIGHT)
		_hand_switched()
	else:
		_hand_left.is_active = false
		_hand_right.is_active = false



func _hand_switched():
	match _character.active_hand:
		Character.Hand.LEFT:
			_hand_left.is_active = true
			_hand_right.is_active = false
		Character.Hand.RIGHT:
			_hand_left.is_active = false
			_hand_right.is_active = true


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
