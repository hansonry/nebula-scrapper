@tool
extends PanelContainer

const active_hand_style : StyleBox = preload("res://ui/style_box_active_hand.tres")

@onready var _item_slot_ui_node : ItemSlotUI = $ItemSlotUI

@export var is_active : bool = false:
	set(value):
		if is_active != value:
			if value:
				add_theme_stylebox_override("panel", active_hand_style)
			else:
				remove_theme_stylebox_override("panel")
		is_active = value

func get_item_slot_ui() -> ItemSlotUI:
	return _item_slot_ui_node

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
