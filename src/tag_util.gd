extends Node
class_name TagUtil

static func is_at_least_one_tag_satisfied(requires : Array[StringName], provides : Array[StringName]) -> bool:
	for req : String in requires:
		if provides.has(req):
			return true
	return false
