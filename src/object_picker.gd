extends RayCast3D
class_name ObjectPicker

signal item_entered(item:Node3D)
signal item_exited(item:Node3D)

var selected : Node3D = null

@onready var _camera :Camera3D = get_node("..")
@onready var _stored_target: Vector3 = target_position
@onready var _ray_length:float = target_position.length()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _picker_swiched_to(from_item : Node3D, to_item: Node3D):
	if from_item != null:
		item_exited.emit(from_item)
	if to_item != null:
		item_entered.emit(to_item)

func clear_selected_item():
	if selected != null:
		item_exited.emit(selected)
		selected = null

func _get_hit_item() -> Node3D:
	var obj = get_collider()
	while obj != null and not (obj is ItemNode or obj is BuildableNode or obj.has_method("_ray_input")):
		obj = obj.get_parent()
	if obj != null and obj.is_queued_for_deletion():
		obj = null
	return obj # Either null or an ItemNode at this point
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var hit_obj : Node3D = _get_hit_item()
	if selected != hit_obj:
		_picker_swiched_to(selected, hit_obj)
		selected = hit_obj
		
	if Input.mouse_mode == Input.MOUSE_MODE_VISIBLE:
		var mouse_pos = get_viewport().get_mouse_position()
		target_position = _camera.project_local_ray_normal(mouse_pos) * _ray_length
	else:
		target_position = _stored_target
	pass

func _unhandled_input(event):
	if selected != null and selected.has_method("_ray_input"):
		selected._ray_input(event)

func get_hit_position_or(offset: Vector3, normal_backup : float = 0.0) -> Vector3:
	if is_colliding():
		var collision_point : Vector3 = get_collision_point()
		if (collision_point - global_position).length_squared() < offset.length_squared():
			var global_point: Vector3 = collision_point + get_collision_normal() * normal_backup
			return global_point
	return to_global(position + offset)
