extends Area3D

signal rocker_clicked

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass

func _rocker_clicked():
	rocker_clicked.emit()


func _ray_input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			_rocker_clicked.call_deferred()
