extends Node3D
class_name RockerSwitch

signal switch_changed

const _SWITCH_SPEED : float = 10

@export var on_material : BaseMaterial3D
@export var off_material : BaseMaterial3D

@onready var _animation_player : AnimationPlayer = $AnimationPlayer
@onready var _rocker_mesh : MeshInstance3D = _find_mesh(%rocker_switch_rocker)


@export var on: bool = false:
	set(value):
		if on != value:
			if is_node_ready():
				_switch_to(value)
			on = value
			switch_changed.emit()

func _switch_to(value: bool):
	if value:
		_animation_player.play("rocker", -1, _SWITCH_SPEED)
		_rocker_mesh.set_surface_override_material(0, on_material)
	else:
		_animation_player.play("rocker", -1, -_SWITCH_SPEED, true)
		_rocker_mesh.set_surface_override_material(0, off_material)


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	assert(_rocker_mesh != null)
	_switch_to(on)


static func _find_mesh(node: Node) -> MeshInstance3D:
	for child in node.get_children():
		if child is MeshInstance3D:
			return child
		else:
			return _find_mesh(child)
	return null
			

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass


func _on_switch_area_rocker_clicked() -> void:
	on = not on
