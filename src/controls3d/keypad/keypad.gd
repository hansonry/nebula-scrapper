extends Node3D
class_name Keypad

enum KeypadButton {
	BUTTON_0, BUTTON_1, BUTTON_2, BUTTON_3, BUTTON_4, BUTTON_5, BUTTON_6, BUTTON_7, BUTTON_8, BUTTON_9,
	BUTTON_PERIOD,
	BUTTON_MINUS,
	BUTTON_BACK,
	BUTTON_RETURN
}

signal on_pressed(button: Keypad.KeypadButton)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _button_pressed(button: Keypad.KeypadButton):
	#print("Pressed: ", Keypad.KeypadButton.keys()[button])
	on_pressed.emit(button)


func _on_keypad_button_7_on_pressed():
	_button_pressed(Keypad.KeypadButton.BUTTON_7)


func _on_keypad_button_8_on_pressed():
	_button_pressed(Keypad.KeypadButton.BUTTON_8)


func _on_keypad_button_9_on_pressed():
	_button_pressed(Keypad.KeypadButton.BUTTON_9)


func _on_keypad_button_minus_on_pressed():
	_button_pressed(Keypad.KeypadButton.BUTTON_MINUS)


func _on_keypad_button_4_on_pressed():
	_button_pressed(Keypad.KeypadButton.BUTTON_4)


func _on_keypad_button_1_on_pressed():
	_button_pressed(Keypad.KeypadButton.BUTTON_1)


func _on_keypad_button_0_on_pressed():
	_button_pressed(Keypad.KeypadButton.BUTTON_0)


func _on_keypad_button_2_on_pressed():
	_button_pressed(Keypad.KeypadButton.BUTTON_2)


func _on_keypad_button_3_on_pressed():
	_button_pressed(Keypad.KeypadButton.BUTTON_3)


func _on_keypad_button_period_on_pressed():
	_button_pressed(Keypad.KeypadButton.BUTTON_PERIOD)


func _on_keypad_button_5_on_pressed():
	_button_pressed(Keypad.KeypadButton.BUTTON_5)


func _on_keypad_button_6_on_pressed():
	_button_pressed(Keypad.KeypadButton.BUTTON_6)


func _on_keypad_button_back_on_pressed():
	_button_pressed(Keypad.KeypadButton.BUTTON_BACK)


func _on_keypad_button_return_on_pressed():
	_button_pressed(Keypad.KeypadButton.BUTTON_RETURN)
