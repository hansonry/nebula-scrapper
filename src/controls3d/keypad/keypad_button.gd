extends Area3D
class_name KeypadButton

@onready var _animation_player : AnimationPlayer = $AnimationPlayer

signal on_pressed()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _trigger_button_pressed():
	on_pressed.emit()
	_animation_player.play("press",-1, 4)

func _ray_input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			_trigger_button_pressed()

func _on_input_event(camera, event, position, normal, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed:
			_trigger_button_pressed()
