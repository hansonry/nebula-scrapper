extends Object
class_name ExternalConnectorNetwork

signal node_added(node: BuildableNode)
signal node_removed(node: BuildableNode)

var _type : ConnectorNetwork.Type

var _nodes : Array[BuildableNode]

func _init(type: ConnectorNetwork.Type = ConnectorNetwork.Type.PIPE):
	_type = type

func add(node: BuildableNode):
	_nodes.push_back(node)
	node_added.emit(node)


func is_next_to(node: BuildableNode) -> bool:
	for cnn: BuildableNode in _nodes:
		if cnn != node and cnn.has_connection_with(node, _type):
			return true
	return false

func merge_into(net: ExternalConnectorNetwork):
	for node : BuildableNode in net._nodes:
		_nodes.push_back(node)
		node_added.emit(node)

func has_node(node:BuildableNode) -> bool:
	var index : int = _nodes.find(node)
	return index >= 0

func get_connected_nodes(node: BuildableNode) -> Array[BuildableNode]:
	var out : Array[BuildableNode]
	for cnn: BuildableNode in _nodes:
		if cnn != node and cnn.has_connection_with(node, _type):
			out.push_back(cnn)
	return out

func size() -> int:
	return _nodes.size()

func clear() -> void:
	var to_remove_list : Array[BuildableNode]
	for node: BuildableNode in _nodes:
		to_remove_list.push_back(node)
	_nodes.resize(0)
	for node: BuildableNode in to_remove_list:
		node_removed.emit(node)

func remove(node : BuildableNode):
	var index : int = _nodes.find(node)
	if index >= 0:
		_nodes.remove_at(index)
		node_removed.emit(node)

func _swap_nodes(a: int, b: int):
	if a != b:
		var temp: BuildableNode = _nodes[a]
		_nodes[a] = _nodes[b]
		_nodes[b] = temp


func split(new_networks_callback: Callable) -> Array[ExternalConnectorNetwork]:
	var out : Array[ExternalConnectorNetwork]
	var group_start_list: Array[int]
	
	var index: int = 0
	while index < _nodes.size():
		var dest_index : int = index + 1
		group_start_list.push_back(index)
		while index < dest_index:
			for check_index: int in range(dest_index, _nodes.size()):
				if _nodes[index].has_connection_with(_nodes[check_index], _type):
					_swap_nodes(dest_index, check_index)
					dest_index += 1
			index += 1
	out.resize(group_start_list.size() - 1)
	if out.size() > 0:
		for gs_index: int in range(1, group_start_list.size()):
			var out_index : int = gs_index - 1
			out[out_index] = ExternalConnectorNetwork.new(_type)

		new_networks_callback.call(out)
		
		for gs_index: int in range(1, group_start_list.size()):
			var out_index : int = gs_index - 1
			var start_index : int = group_start_list[gs_index]
			var end_index: int = _nodes.size()
			if gs_index < group_start_list.size() - 1:
				end_index = group_start_list[gs_index + 1]
			var network :ExternalConnectorNetwork = out[out_index] 
			for node_index : int in range(start_index, end_index):
				network.add(_nodes[node_index])

		# Remove everything off the end of this network
		if group_start_list.size() > 1:
			var to_remove_list : Array[BuildableNode]
			var end_index : int = group_start_list[1]
			for i : int in range(end_index, _nodes.size()):
				to_remove_list.push_back(_nodes[i])
			_nodes.resize(end_index)
			for node: BuildableNode in to_remove_list:
				node_removed.emit(node)
	return out
