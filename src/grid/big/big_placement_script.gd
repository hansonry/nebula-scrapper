extends Object
class_name BigPlacementScript

signal changed

var facing : BigMap.Slot = BigMap.Slot.FRONT :
	set(value):
		if value != facing:
			facing = value
			changed.emit()
				

var _last_big_position : Vector3i = Vector3i.ZERO

var _big_buildable : BigBuildable

func set_buildable(buildable: Buildable):
	_big_buildable = buildable

func transfer_state(obj: Object):
	if obj is BigPlacementScript:
		facing = obj.facing

func set_transform(node: Node3D):
	node.transform = BigMap.make_transform(Vector3.ZERO, facing)

func orientation_next():
	if facing == BigMap.Slot.DOWN:
		facing = BigMap.Slot.FRONT
	else:
		@warning_ignore("int_as_enum_without_cast")
		facing += 1

func orientation_previous():
	if facing == BigMap.Slot.FRONT:
		facing = BigMap.Slot.DOWN
	else:
		@warning_ignore("int_as_enum_without_cast")
		facing -= 1

	

func get_big_position(position : Vector3) -> Vector3i:
	return BigMap.big_position_from_local(position)


func update_position(placement: Placement):
	var global_position : Vector3 = placement.object_picker.get_hit_position_or(Vector3(0, 0, -4), 1)
	var local_position: Vector3 = placement.get_parent_node_3d().to_local(global_position)
	var new_big_position: Vector3i = get_big_position(local_position)
	placement.position = new_big_position * BigMap.GRID_SIZE
	if new_big_position != _last_big_position:
		_last_big_position = new_big_position
		changed.emit()

func place(character : Character, design_index: int) -> Node3D:
	var node : Node3D = character.build_big(_last_big_position, design_index, facing)
	return node

func get_highlight(vessel: Vessel) -> Placement.Highlight:
	if vessel.is_big_slot_blocked(_last_big_position, facing, _big_buildable.type):
		return Placement.Highlight.RED
	return Placement.Highlight.GREEN
