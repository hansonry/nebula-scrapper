extends Buildable
class_name BigBuildable

enum Type {SIDE, WIDE_SIDE, FULL}

@export var type: Type = Type.SIDE
