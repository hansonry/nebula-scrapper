extends Object
class_name BigMap

enum Slot { FULL, FRONT, BACK, LEFT, RIGHT, UP, DOWN }
const GRID_SIZE : float = 4.0

var _big_buildable_nodes : Dictionary = {}
var _big_buildable_nodes_mutex : Mutex
var _volumes : Dictionary = {}
var _volumes_mutex : Mutex

var transform: Transform3D : 
	set(value):
		_inv_transform = value.inverse( )
var _inv_transform : Transform3D

const _other_side_offsets: Array[Vector3i] = [
	Vector3i( 0,  0,  1), # FRONT
	Vector3i( 0,  0, -1), # BACK
	Vector3i( 1,  0,  0), # LEFT
	Vector3i(-1,  0,  0), # RIGHT
	Vector3i( 0, -1,  0), # UP 
	Vector3i( 0,  1,  0), # DOWN
]

const _other_side_slots: Array[Slot] = [
	Slot.BACK,
	Slot.FRONT,
	Slot.RIGHT,
	Slot.LEFT,
	Slot.DOWN,
	Slot.UP
]

const _slot_to_link_name: Array[String] = [
	"link_zp",
	"link_zn",
	"link_xp",
	"link_xn",
	"link_yn",
	"link_yp",
]


const _neighbors_offsets : Array[Vector3i] = [
	Vector3i( 1,  0,  0),
	Vector3i(-1,  0,  0),
	Vector3i( 0,  1,  0),
	Vector3i( 0, -1,  0),
	Vector3i( 0,  0,  1),
	Vector3i( 0,  0, -1),
]

const _my_link_names : Array[String] = [
	"link_xp",
	"link_xn",
	"link_yp",
	"link_yn",
	"link_zp",
	"link_zn"
]

const _their_link_names : Array[String] = [
	"link_xn",
	"link_xp",
	"link_yn",
	"link_yp",
	"link_zn",
	"link_zp"
]

const _blocking_slot_lookup : Array[Slot] = [
	# My Slot, Their Slot
	Slot.LEFT,  Slot.RIGHT,
	Slot.RIGHT, Slot.LEFT,
	Slot.DOWN,  Slot.UP,
	Slot.UP,    Slot.DOWN,
	Slot.FRONT, Slot.BACK,
	Slot.BACK,  Slot.FRONT,
]

func _init():
	_volumes_mutex = Mutex.new()
	_big_buildable_nodes_mutex = Mutex.new()
	GameAtmosSystem.add_grid_base(self)

func detach():
	GameAtmosSystem.remove_grid_base(self)


func has_slot(big_position: Vector3i, slot : Slot) -> bool:
	var has_slot :bool = false
	_big_buildable_nodes_mutex.lock()
	has_slot = _big_buildable_nodes.has(big_position) and _big_buildable_nodes[big_position][slot] != null
	_big_buildable_nodes_mutex.unlock()
	return has_slot
	
func is_slot_blocked(big_position: Vector3i, slot : Slot, type: BigBuildable.Type) -> bool:
	var is_slot_blocked : bool = false
	_big_buildable_nodes_mutex.lock()
	if not has_slot(big_position, Slot.FULL):
		if has_slot(big_position, slot):
			is_slot_blocked = true
		else:
			var index : int = slot - 1;
			var other_big : Vector3i = big_position + _other_side_offsets[index]
			var other_slot : Slot = _other_side_slots[index]
			if has_slot(other_big, other_slot) and (get_slot(other_big, other_slot).buildable.type == BigBuildable.Type.WIDE_SIDE or type == BigBuildable.Type.WIDE_SIDE):
				is_slot_blocked = true
	_big_buildable_nodes_mutex.unlock()
	return is_slot_blocked
	
func get_slot(big_position: Vector3i, slot : Slot) -> BigBuildableNode:
	var node: BigBuildableNode = null
	_big_buildable_nodes_mutex.lock()
	if _big_buildable_nodes.has(big_position):
		node = _big_buildable_nodes[big_position][slot]
	_big_buildable_nodes_mutex.unlock()
	return node
	

func _get_or_create_slot_array_at(big_position: Vector3i) -> Array[BigBuildableNode]:
	if _big_buildable_nodes.has(big_position):
		return _big_buildable_nodes[big_position]
	var array : Array[BigBuildableNode] = []
	array.resize(Slot.size())
	array.fill(null)
	_big_buildable_nodes[big_position] = array
	return array
	
func set_slot(big_position: Vector3i, slot : Slot, node: BigBuildableNode):
	_big_buildable_nodes_mutex.lock()
	var array : Array[BigBuildableNode] = _get_or_create_slot_array_at(big_position)
	array[slot] = node
	node.step_changed.connect(func():
		GameAtmosSystem.queue_callable(func(atmos_system:AtmosSystem):
			var big_volume: AtmosVolumeBig = _get_big_volume(big_position)
			if big_volume != null:
				_update_blocking(big_volume)
		)
	)
	_big_buildable_nodes_mutex.unlock()
	_add_atmos_if_nessary(big_position)

func clear_slot(big_position: Vector3i, slot : Slot):
	_big_buildable_nodes_mutex.lock()
	if _big_buildable_nodes.has(big_position):
		_big_buildable_nodes[big_position][slot] = null
	_big_buildable_nodes_mutex.unlock()


func _add_neighbors_if_nessary(big_position: Vector3i, atmos_system: AtmosSystem, volume_big: AtmosVolumeBig):
	for i in range(_neighbors_offsets.size()):
		var neighbor_position: Vector3i = big_position + _neighbors_offsets[i]
		var my_link_name : String = _my_link_names[i]
		var their_link_name : String = _their_link_names[i]
		if volume_big.get(my_link_name) == null:
			var neighbor_volume_big : AtmosVolumeBig = null
			if _volumes.has(neighbor_position):
				neighbor_volume_big = _volumes[neighbor_position]
			else:
				neighbor_volume_big = AtmosVolumeBig.new(atmos_system, neighbor_position)
				neighbor_volume_big.is_outside = true
				_volumes[neighbor_position] = neighbor_volume_big
			var link : AtmosLink = atmos_system.link_add(volume_big.volume, neighbor_volume_big.volume)
			link.area_m2 = 4
			volume_big.set(my_link_name, link)
			neighbor_volume_big.set(their_link_name, link)
			

func _get_big_volume(big_position : Vector3i) -> AtmosVolumeBig:
	var big_volume : AtmosVolumeBig = null
	_volumes_mutex.lock()
	if _volumes.has(big_position):
		big_volume = _volumes[big_position]
	_volumes_mutex.unlock()
	return big_volume

func _get_build_step(big_position: Vector3i, slot : Slot) -> BuildStep:
	var node : BigBuildableNode = get_slot(big_position, slot)
	if node != null:
		return node.get_current_step()
	return null

func _does_building_block_atmos(big_position: Vector3i, slot : Slot) -> bool:
	var build_step : BuildStep = _get_build_step(big_position, slot)
	if build_step == null:
		return false
	return build_step.blocks_atmos

func _update_blocking(big_volume : AtmosVolumeBig):
	#print("Update Blocking")
	for i in range(_neighbors_offsets.size()):
		var neighbor_position: Vector3i = big_volume.big_position + _neighbors_offsets[i]
		var my_slot : Slot =  _blocking_slot_lookup[i * 2]
		var their_slot : Slot = _blocking_slot_lookup[(i * 2) + 1]
		var my_link_name : String = _my_link_names[i]
		var my_slot_blocks : bool = _does_building_block_atmos(big_volume.big_position, Slot.FULL) or _does_building_block_atmos(big_volume.big_position, my_slot)
		var their_slot_blocks : bool = _does_building_block_atmos(neighbor_position, Slot.FULL) or _does_building_block_atmos(neighbor_position, their_slot)
		var block: bool = my_slot_blocks or their_slot_blocks
		
		var link: AtmosLink = big_volume.get(my_link_name)
		#print("Link: ", big_volume.big_position, "  ", my_link_name,"  ", block)
		if block:
			link.set_closed()
		else:
			link.set_open()
			

func _add_atmos_if_nessary(big_position: Vector3i):
	GameAtmosSystem.queue_callable(func(atmos_system:AtmosSystem):
		_volumes_mutex.lock()
		var volume_big: AtmosVolumeBig = null
		if _volumes.has(big_position):
			volume_big = _volumes[big_position]
			volume_big.is_outside = false
		else:
			volume_big = AtmosVolumeBig.new(atmos_system, big_position)
			_volumes[big_position] = volume_big
		_add_neighbors_if_nessary(big_position, atmos_system, volume_big)
		_update_blocking(volume_big)
		_volumes_mutex.unlock()
	)

func to_local(l_global_point: Vector3) -> Vector3:
	var result: Vector3 = _inv_transform * l_global_point
	return result


func to_local_big(l_global_point: Vector3) -> Vector3i:
	var local: Vector3 = to_local(l_global_point)
	var big_position : Vector3i = BigMap.big_position_from_local(local)
	return big_position

func contains_global_point(l_global_point: Vector3) -> bool:
	var big_position : Vector3i = to_local_big(l_global_point)
	_volumes_mutex.lock()
	var result: bool = _volumes.has(big_position)
	_volumes_mutex.unlock()
	return result

func get_sample_at_global_position(l_global_position: Vector3, sample: AtmosSample = null) -> AtmosSample:
	var big_position : Vector3i = to_local_big(l_global_position)
	return get_sample_at_big_position(big_position, sample)

func get_sample_at_big_position(big_position: Vector3i, sample: AtmosSample = null) -> AtmosSample:
	if sample == null:
		sample = AtmosSample.new()
	var volume: AtmosVolume = get_volume_at_big_position(big_position)
	if volume == null:
		return null
	sample.set_from_volume(volume)
	return sample
	

func get_volume_at_global_position(l_global_position: Vector3i) -> AtmosVolume:
	var big_position : Vector3i = to_local_big(l_global_position)
	return get_volume_at_big_position(big_position)
	
func get_volume_at_big_position(big_position: Vector3i) -> AtmosVolume:
	var result: AtmosVolume = null
	_volumes_mutex.lock()
	if _volumes.has(big_position):
		result = _volumes[big_position].volume
	_volumes_mutex.unlock()
	return result
	
func get_slot_atmos_link(big_position: Vector3i, slot : Slot) -> AtmosLink:
	var big_volume : AtmosVolumeBig = null
	_volumes_mutex.lock()
	if _volumes.has(big_position):
		big_volume = _volumes[big_position]
	_volumes_mutex.unlock()
	if big_volume == null:
		return null
	return big_volume.get(_slot_to_link_name[slot - 1])

static func big_position_from_local(local: Vector3) -> Vector3i:
	var hg : float = GRID_SIZE / 2.0
	return floor((local + Vector3(hg, hg, hg) )/ GRID_SIZE)

static func make_transform(big_position: Vector3i, slot : Slot) -> Transform3D:
	var o_rotation : Vector3
	var o_position : Vector3 = big_position * GRID_SIZE
	const a90 = PI / 2
	const s = GRID_SIZE
	match slot:
		Slot.FRONT:
			o_rotation = Vector3(a90, 0, 2 * a90)
		Slot.BACK:
			o_rotation = Vector3(a90, 0, 0)
		Slot.LEFT:
			o_rotation = Vector3(a90, -a90, 0)
		Slot.RIGHT:
			o_rotation = Vector3(a90, a90, 0)
		Slot.UP, Slot.FULL:
			o_rotation = Vector3.ZERO
		Slot.DOWN:
			o_rotation = Vector3(2 * a90, 0, 0)
	return Transform3D(Basis.from_euler(o_rotation), o_position)
