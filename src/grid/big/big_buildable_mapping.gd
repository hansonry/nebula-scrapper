extends Resource
class_name BigBuildableMapping

@export var map : Array[Buildable] = []

var _reverse_map : Dictionary = {}

func get_buildable(index : int) -> Buildable:
	if index < 0:
		return null
	return map[index]

func _fill_reverse_map():
	for index in map.size():
		var buildable: Buildable = map[index]
		_reverse_map[buildable] = index
		

func get_index(buildable: Buildable) -> int:
	if _reverse_map.size() < map.size():
		_fill_reverse_map()
	return _reverse_map[buildable]
