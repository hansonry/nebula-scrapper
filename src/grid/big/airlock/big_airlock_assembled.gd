extends Node3D


@onready var _door : AnimatableBody3D = $DoorAnimatableBody3D

var is_open: bool = false:
	set(value):
		_door.is_open = value
	get:
		return _door.is_open

var _atmos_link : AtmosLink = null

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var vessel: Vessel = Vessel.find(self)
	assert(vessel != null)
	var big_buildable_node : BigBuildableNode = BigBuildableNode.find(self)
	assert(big_buildable_node != null)
	GameAtmosSystem.queue_callable(func (atmos_system: AtmosSystem):
		_atmos_link = vessel.get_big_slot_atmos_link(big_buildable_node.big_position, big_buildable_node.slot)
		if _atmos_link != null:
			_atmos_link.set_closed()
	)
	_door.state_changed.connect( func():
		var local_is_open: bool = not _door.is_sealed()
		GameAtmosSystem.queue_callable(func (atmos_system: AtmosSystem):
			if _atmos_link == null:
				_atmos_link = vessel.get_big_slot_atmos_link(big_buildable_node.big_position, big_buildable_node.slot)
			assert(_atmos_link != null)
			if local_is_open:
				_atmos_link.set_open()
			else:
				_atmos_link.set_closed()
		)
	)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass
