extends AnimatableBody3D

enum State {CLOSED, OPENING, OPEN, CLOSING}

@onready var _animation : AnimationPlayer = $"../AnimationPlayer"

signal desired_open_changed()
signal state_changed()

var _state : State = State.CLOSED
var state: State:
	get:
		return _state

var desired_open : bool = false :
	set(value):
		if desired_open != value:
			desired_open = value
			if desired_open:
				_state = State.OPENING
				state_changed.emit()
				_animation.play("airlock_door_open")
				
			else:
				_state = State.CLOSING
				state_changed.emit()
				_animation.play("airlock_door_open", -1, -1, true)
			desired_open_changed.emit()
		

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass

func is_sealed():
	return _state == State.CLOSED


func _door_activated():
	desired_open = not desired_open
	
func _ray_input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			_door_activated()


func _on_animation_player_animation_finished(anim_name: StringName) -> void:
	if desired_open:
		_state = State.OPEN
		state_changed.emit()
	else:
		_state = State.CLOSED
		state_changed.emit()
