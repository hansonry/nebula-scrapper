extends BuildableNode
class_name BigBuildableNode

@export var big_position : Vector3i
@export var slot: BigMap.Slot

func _remove_from_grid():
	var vessel : Vessel = Vessel.find(self)
	assert(vessel != null)
	vessel.clear_big_slot(big_position, slot)

func _ready():
	super()
	var vessel : Vessel = Vessel.find(self)
	assert(vessel != null)
	vessel.set_big_slot(big_position, slot, self)


static func find(node: Node3D):
	while node != null and not node is BigBuildableNode:
		node = node.get_parent_node_3d()
	return node
