extends Object
class_name ExternalConnectorNetworkManager

signal network_added(network: ExternalConnectorNetwork)
signal network_removed(network: ExternalConnectorNetwork)
signal network_post_split(seed_network : ExternalConnectorNetwork, new_networks : ExternalConnectorNetwork)

var _type : ConnectorNetwork.Type

var _networks :Array[ExternalConnectorNetwork]

func _init(type: ConnectorNetwork.Type = ConnectorNetwork.Type.PIPE):
	_type = type

func _remove_by_network(network: ExternalConnectorNetwork):
	var index : int = _networks.find(network)
	if index >= 0:
		_networks.remove_at(index)
		network.clear()
		network_removed.emit(network)


func _merge_networks(networks: Array[ExternalConnectorNetwork]):
	for i in range(1, networks.size()):
		networks[0].merge_into(networks[i])
		_remove_by_network(networks[i])
		

func add(node: BuildableNode):
	for cn : ConnectorNetwork in node.buildable.network_set.network_list:
		if cn.type == _type:
			var next_to_list: Array[ExternalConnectorNetwork]
			for network : ExternalConnectorNetwork in _networks:
				if network.is_next_to(node):
					next_to_list.push_back(network)
			if next_to_list.size() == 0:
				var network : ExternalConnectorNetwork = ExternalConnectorNetwork.new(_type)
				_networks.push_back(network)
				network_added.emit(network)
				network.add(node)
			else:
				next_to_list[0].add(node)
				_merge_networks(next_to_list)

func _find_network_that_has_node(node : BuildableNode) -> ExternalConnectorNetwork:
	for network in _networks:
		if network.has_node(node):
			return network
	return null

func remove(node: BuildableNode):
	var network : ExternalConnectorNetwork = _find_network_that_has_node(node)
	if network != null:
		if network.size() == 1:
			_remove_by_network(network)
		else:
			network.remove(node)
			var new_networks : Array[ExternalConnectorNetwork] = network.split(func(new_networks: Array[ExternalConnectorNetwork]):
				for nn in new_networks:
					_networks.push_back(nn)
					network_added.emit(nn)
			)
			if new_networks.size() > 0:
				network_post_split.emit(network, new_networks)


func find_network_containing(node : BuildableNode) -> ExternalConnectorNetwork:
	for network in _networks:
		if network.has_node(node):
			return network
	return null
