extends Node3D
class_name Placement

const _material_green  : Material = preload("res://grid/material_placement_green.tres")
const _material_yellow : Material = preload("res://grid/material_placement_yellow.tres")
const _material_red    : Material = preload("res://grid/material_placement_red.tres")

enum Highlight { GREEN, YELLOW, RED }

var highlight : Highlight = Highlight.GREEN:
	set(value):
		highlight = value
		_update_mesh_highlight()

var _script_object = null

@export var character : Character = null:
	set(value):
		if character != value:
			character = value
			_update_preview_node()

@export var object_picker : ObjectPicker = null

var _script_lookup: Dictionary = {}

var _mesh_list : Array[MeshInstance3D] = []

@export var design_index : int = 0:
	set(value):
		if value < 0:
			value = 0
		if value >= buildables.size():
			value = buildables.size() - 1
		if value != design_index:
			design_index = value
			if is_node_ready():
				_update_preview_node()


@export var buildables : Array[Buildable]:
	set(value):
		if value != buildables:
			buildables = value
			design_index = 0
			if is_node_ready():
				_update_preview_node()

var _preview_node : Node3D = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _get_script_object_from_buildable(buildable: Buildable, prev_script_object: Object = null) -> Object:
	var script: Object = null
	if _script_lookup.has(buildable):
		script = _script_lookup[buildable]
	else:
		script = buildable.make_placement_script()
		_script_lookup[buildable] = script
	if prev_script_object != null:
		script.transfer_state(prev_script_object)
	return script


func _update_preview_node():
	if character != null and buildables.size() > 0:
		if _preview_node != null:
			_mesh_list.clear()
			_preview_node.queue_free()
			_preview_node = null
			_script_object.changed.disconnect(_on_script_change)
		_preview_node = buildables[design_index].make_preview_scene()
		_script_object = _get_script_object_from_buildable(buildables[design_index], _script_object)
		_script_object.changed.connect(_on_script_change)
		add_child(_preview_node)
		_lookup_stuff(_preview_node)
		assert(_mesh_list.size() > 0)
		_update_highlight_based_on_position_and_orientation()
		_update_mesh_highlight()
		_update_orientation()


func _on_script_change():
	_update_highlight_based_on_position_and_orientation()
	_update_mesh_highlight()
	_update_orientation()

func _lookup_stuff(node):
	if node is MeshInstance3D:
		_mesh_list.append(node)
	for child in node.get_children():
		_lookup_stuff(child)

func _update_mesh_highlight():
	var material : Material = null
	match highlight:
		Highlight.RED:
			material = _material_red
		Highlight.YELLOW:
			material = _material_yellow
		Highlight.GREEN:
			material = _material_green
	
	for mesh: MeshInstance3D in _mesh_list:
		mesh.set_surface_override_material(0, material)

func _update_orientation():
	_script_object.set_transform(_preview_node)

func orientation_next():
	_script_object.orientation_next()

func orientation_previous():
	_script_object.orientation_previous()

func design_next():
	var temp_index : int = design_index
	temp_index += 1
	if temp_index >= buildables.size():
		temp_index = 0
	design_index = temp_index

func design_previous():
	var temp_index : int = design_index
	temp_index -= 1
	if temp_index < 0:
		temp_index = buildables.size() - 1
	design_index = temp_index

func place() -> Node3D:
	var node: Node3D = _script_object.place(character, design_index)
	hide()
	return node

func _update_highlight_based_on_position_and_orientation():
	var vessel = character.find_vessel()
	assert(vessel != null)
	highlight = _script_object.get_highlight(vessel)


	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if visible:
		_script_object.update_position(self)
		if Input.is_action_just_released("DesignNext"):
			design_next()
		if Input.is_action_just_pressed("DesignPrevious"):
			design_previous()
		if Input.is_action_just_pressed("RotatePlacement"):
			orientation_next()


func _on_visibility_changed() -> void:
	if visible:
		_update_preview_node()
