extends Object
class_name SmallPlacementScript

signal changed

var _last_small_position : Vector3i = Vector3i.ZERO

var _buildable: SmallBuildable = null

var _orientation_list: Array

var _snap_list : Array[bool]

var _orientation_index: int = 0:
	set(value):
		if value >= _orientation_list.size():
			value = 0
		elif value < 0:
			value = _orientation_list.size() - 1
			
		if value != _orientation_index:
			_orientation_index = value
			changed.emit()
			

func _create_full_orientation_set() -> Array[Basis]:
	const _angles : Array[float] = [
		deg_to_rad(0),
		deg_to_rad(90),
		deg_to_rad(180),
		deg_to_rad(270)
	]
	var orientation_set : Array[Basis]
	orientation_set.resize( 64)
	var index : int = 0
	for x_angle in _angles:
		for y_angle in _angles:
			for z_angle in _angles:
				orientation_set[index] = Basis.from_euler(Vector3(x_angle, y_angle, z_angle), EULER_ORDER_ZYX)
				index += 1
	return orientation_set

func _create_orientation_set(buildable: Buildable) -> Array[Basis]:
	return _create_full_orientation_set()



func set_buildable(buildable: Buildable):
	_buildable = buildable
	if _buildable.network_set != null:
		_buildable.network_set.assert_check()
		
	_orientation_list = _create_orientation_set(_buildable)
	_snap_list.resize(0)
	if _orientation_index >= _orientation_list.size():
		_orientation_index = 0



func transfer_state(obj: Object):
	pass

func set_transform(node: Node3D):
	node.transform = SmallMap.make_transform(Vector3.ZERO, _orientation_list[_orientation_index])


func _index_delta_wrap(index: int, array_size: int, delta: int) -> int:
	var new_index : int = index + delta
	if delta > 0 and new_index >= array_size:
		new_index = 0
	elif delta < 0 and new_index < 0:
		new_index = array_size - 1
	return new_index

func _next_snap_index_delta(delta: int):
	if _snap_list.size() <= 0:
		_orientation_index += delta
	else:
		var size :int = _orientation_list.size()
		var next_index:int = _index_delta_wrap(_orientation_index, size, delta)
		while next_index != _orientation_index and !_snap_list[next_index]:
			next_index = _index_delta_wrap(next_index, size, delta)
		if next_index == _orientation_index:
			if !_snap_list[next_index]:
				_orientation_index += delta
		else:
			_orientation_index = next_index


func orientation_next():
	_next_snap_index_delta(1)
		

func orientation_previous():
	_next_snap_index_delta(-1)
		

func _position_changed(new_small_position: Vector3i):
	_last_small_position = new_small_position
	changed.emit()

func get_small_position(position : Vector3) -> Vector3i:
	return SmallMap.small_position_from_local(position)

func update_position(placement: Placement):
	var global_position : Vector3 = placement.object_picker.get_hit_position_or(Vector3(0, 0, -2.5), 0.1)
	var local_position: Vector3 = placement.get_parent_node_3d().to_local(global_position)
	var new_small_position: Vector3i = get_small_position(local_position)
	placement.position = new_small_position * SmallMap.GRID_SIZE
	if new_small_position != _last_small_position:
		_position_changed(new_small_position)



func place(character : Character, design_index: int) -> Node3D:
	var node : Node3D = character.build_small(_last_small_position, _orientation_list[_orientation_index], design_index)
	return node


func get_highlight(vessel: Vessel) -> Placement.Highlight:
	if vessel.has_small_node(_last_small_position, _buildable.layer):
		return Placement.Highlight.RED
	return Placement.Highlight.GREEN
