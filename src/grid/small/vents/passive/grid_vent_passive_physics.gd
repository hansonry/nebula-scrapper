extends StaticBody3D

const VENT_FACE_NORMAL : Vector3 = Vector3(0, 1, 0)

var _small_buildable_node: SmallBuildableNode
var _previous_ext_net : ExternalConnectorNetwork = null

var _link : AtmosLink


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	_small_buildable_node = SmallBuildableNode.find(self)
	assert(_small_buildable_node != null)
	_small_buildable_node.ready.connect(_on_buildable_ready)
	
func _on_buildable_ready():
	_link_to_big_volume()
func _link_to_big_volume():
	var vessel : Vessel = Vessel.find(_small_buildable_node)
	assert(vessel != null)
	var ext_net: ExternalConnectorNetwork = vessel.get_external_network(_small_buildable_node, &"connection")
	assert(ext_net != null)
	ext_net.node_removed.connect(_ext_network_node_removed)
	var vent_facing :Vector3 = VENT_FACE_NORMAL * _small_buildable_node.basis
	
	GameAtmosSystem.queue_callable(func(system: AtmosSystem):
		var pipe_volume: AtmosVolume = vessel.get_pipe_volume_from_network(ext_net)
		assert(pipe_volume != null)
		
	
		var big_volume: AtmosVolume = GameAtmosSystem.get_volume_at(global_position + vent_facing * 0.5)
		assert(big_volume != null)
		_link = system.link_add(pipe_volume, big_volume)
	)
	_previous_ext_net = ext_net
	
func _ext_network_node_removed(node: BuildableNode):
	if node == _small_buildable_node:
		_remove_link_to_big_volume()
		_link_to_big_volume()
	
func _exit_tree() -> void:
	_remove_link_to_big_volume()

func _remove_link_to_big_volume() -> void:
	if _previous_ext_net != null:
		_previous_ext_net.node_removed.disconnect(_ext_network_node_removed)
		_previous_ext_net = null
	GameAtmosSystem.queue_callable(func (system:AtmosSystem):
		if _link != null:
			system.link_remove(_link)
			_link = null
	)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass
