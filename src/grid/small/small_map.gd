extends Object
class_name SmallMap

enum Layer {NORMAL, PIPE, WIRE}

const GRID_SIZE : float = 1.0

var _nodes: Array[Dictionary]

func _init():
	_nodes.resize(Layer.size())
	for layer in Layer.values():
		_nodes[layer] = {}


func set_node(small_position : Vector3i, layer: Layer, node : SmallBuildableNode):
	_nodes[layer][small_position] = node

func get_node(small_position: Vector3i, layer: Layer) -> SmallBuildableNode:
	if has_node(small_position, layer):
		return _nodes[layer][small_position]
	return null

func has_node(small_position: Vector3i, layer :Layer) -> bool:
	return _nodes[layer].has(small_position)

func clear_node(small_position: Vector3i, layer: Layer):
	if _nodes[layer].has(small_position):
		_nodes[layer].erase(small_position)

func get_connection_with_count(small_position: Vector3i,  network_set: ConnectorNetworkSet) -> int:
	var count : int = 0
	for network : ConnectorNetwork in network_set.network_list:
		for connection: Connector in network.connector_list:
			var target_small_position : Vector3i = small_position + connection.offset + connection.direction
			for grid in _nodes:
				if grid.has(target_small_position):
					var target_node = grid[target_small_position]
					for target_network: ConnectorNetwork in target_node.network_set.network_list:
						if target_network.type == network.type:
							for xconnection : Connector in  target_network.connector_list:
								var xform_small_position : Vector3i = target_node.small_position + xconnection.offset + xconnection.direction
								var direction_sum : Vector3i = xconnection.direction + connection.direction
								if xform_small_position == small_position and direction_sum == Vector3i.ZERO:
									count += 1
	return count


func has_connection_with(small_position: Vector3i, network_set: ConnectorNetworkSet) -> bool:
	for network : ConnectorNetwork in network_set.network_list:
		for connection: Connector in network.connector_list:
			var target_small_position : Vector3i = small_position + connection.offset + connection.direction
			for grid in _nodes:
				if grid.has(target_small_position):
					var target_node = grid[target_small_position]
					for target_network: ConnectorNetwork in target_node.network_set.network_list:
						if target_network.type == network.type:
							for xconnection : Connector in  target_network.connector_list:
								var xform_small_position : Vector3i = target_node.small_position + xconnection.offset + xconnection.direction
								var direction_sum : Vector3i = xconnection.direction + connection.direction
								if xform_small_position == small_position and direction_sum == Vector3i.ZERO:
									return true
	return false


static func small_position_from_local(local: Vector3) -> Vector3i:
	var hg : float = GRID_SIZE / 2.0
	return floor((local + Vector3(hg, hg, hg) )/ GRID_SIZE)

static func make_transform(small_position: Vector3i, basis: Basis) -> Transform3D:
	var o_position : Vector3 = small_position * GRID_SIZE
	return Transform3D(basis, o_position)
