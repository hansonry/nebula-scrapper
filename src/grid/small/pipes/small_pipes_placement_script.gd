extends SmallPlacementScript
class_name SmallPipePlacementScript

func _does_connection_list_match(a: ConnectorNetworkSet, b: ConnectorNetworkSet) -> bool:
	for net_index: int in range(a.network_list.size()):
		var cna : ConnectorNetwork = a.network_list[net_index]
		var cnb : ConnectorNetwork = b.network_list[net_index]
		for ca:Connector in cna.connector_list:
			var found : bool = false
			for cb: Connector in cnb.connector_list:
				if ca.matches(cb):
					found = true
					break
			if not found:
				return false
	return true

func _make_connector_string(a : Array[Connector]) -> String:
	var str : String = ""
	for connector in a:
		str = str + "(%d, %d, %d), " % [connector.direction.x, connector.direction.y, connector.direction.z]
	return str


func _create_orientation_set(buildable: Buildable) -> Array[Basis]:
	var orientation_set : Array[Basis] = _create_full_orientation_set()
	var i_up: int = 0
	while i_up < orientation_set.size():
		#print("i_up: ", i_up)
		var network_set_up : ConnectorNetworkSet = buildable.create_transformed_network_set(orientation_set[i_up])
		#print("Connector: ", _make_connector_string(connections_up))
		for i_down: int in range(orientation_set.size() - 1, i_up, -1):
			#print("i_down: ", i_down)
			var network_set_down:ConnectorNetworkSet = buildable.create_transformed_network_set(orientation_set[i_down])
			if _does_connection_list_match(network_set_up, network_set_down):
				orientation_set.remove_at(i_down)
				#print("Removed Orientation: ", i_down)
		i_up += 1
	print("Orientation Set Size: ", orientation_set.size())
	return orientation_set




func set_buildable(buildable: Buildable):
	super(buildable)

func transfer_state(obj: Object):
	if obj is SmallPipePlacementScript:
		pass

func _position_changed(new_small_position: Vector3i):
	_snap_list.resize(0)
	super(new_small_position)


func _has_connection_at(vessel: Vessel, orientation_index: int) -> bool:
		var network_set:ConnectorNetworkSet = _buildable.create_transformed_network_set( _orientation_list[orientation_index])
		return vessel.has_small_connection_with(_last_small_position, network_set)

func _get_connection_counts_at(vessel: Vessel, orientation_index: int) -> int:
		var network_set:ConnectorNetworkSet = _buildable.create_transformed_network_set( _orientation_list[orientation_index])
		return vessel.get_small_connection_with_count(_last_small_position, network_set)

func _update_snap_list(vessel: Vessel):
	if _snap_list.size() == 0:
		var counts : Array[int]
		_snap_list.resize(_orientation_list.size())
		counts.resize(_orientation_list.size())
		for index in range(_orientation_list.size()):
			counts[index] = _get_connection_counts_at(vessel, index)
		var max_connections : int = counts.max()
		#print("max connections: ", max_connections)
		for index in range(_orientation_list.size()):
			_snap_list[index] = counts[index] == max_connections

func get_highlight(vessel: Vessel) -> Placement.Highlight:
	_update_snap_list(vessel)
	var highlight :Placement.Highlight = super(vessel)
	if highlight == Placement.Highlight.GREEN:
		if _has_connection_at(vessel, _orientation_index):
			highlight = Placement.Highlight.YELLOW
	
	return highlight
