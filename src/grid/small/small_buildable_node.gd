extends BuildableNode
class_name SmallBuildableNode

@export var small_position: Vector3i

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	super()
	var vessel : Vessel = Vessel.find(self)
	assert(vessel != null)
	vessel.set_small_node(small_position, buildable.layer, self)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass

func _remove_from_grid():
	var vessel : Vessel = Vessel.find(self)
	assert(vessel != null)
	vessel.clear_small_node(small_position, buildable.layer)


static func find(node: Node3D):
	while node != null and not node is SmallBuildableNode:
		node = node.get_parent_node_3d()
	return node
