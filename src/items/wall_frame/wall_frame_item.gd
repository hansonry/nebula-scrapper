extends ItemNode

@onready var _mesh : MeshInstance3D = $Frame

# Called when the node enters the scene tree for the first time.
func _ready():
	super()


func _set_material(material :Material):
	_mesh.set_surface_override_material(0, material)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass



