extends Control
class_name AtmosScreen

const TABLET_ATMOS_MOL_UI : PackedScene = preload("res://items/tablet/tablet_atmos_mol_ui.tscn")

const ATMOS_CONFIG : AtmosConfig = preload("res://atmos/atmos_config.tres")

@export var pipe_scanner : NodePath

@onready var _pipe_scanner_node : PipeScanner = get_node(pipe_scanner)
@onready var _mols_container: Container = %MolsContainer
@onready var _label_network_id_value : Label = %LabelNetworkIDValue
@onready var _label_volume_value : Label = %LabelVolumeValue
@onready var _label_total_mols_value: Label = %LabelTotalMolsValue
@onready var _label_pressure_value: Label = %LabelPressureValue
@onready var _label_temperature_value: Label = %LabelTemperatureValue

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	assert(_pipe_scanner_node != null)
	assert(_label_network_id_value != null)
	assert(_label_volume_value != null)
	assert(_label_total_mols_value != null)
	assert(_label_pressure_value != null)
	assert(_label_temperature_value != null)
	for molecule in ATMOS_CONFIG.mol_list:
		var mol_ui = TABLET_ATMOS_MOL_UI.instantiate()
		mol_ui.molecule = molecule
		mol_ui.hide()
		_mols_container.add_child(mol_ui)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	for mol_ui in _mols_container.get_children():
		mol_ui.atmos_sample = _pipe_scanner_node.atmos_sample
	if _pipe_scanner_node.atmos_sample != null:
		_label_volume_value.text = "%.01f" % _pipe_scanner_node.volume_l
		_label_network_id_value.text = "%d" % _pipe_scanner_node.network.get_instance_id()
		_label_total_mols_value.text = "%.02f" % _pipe_scanner_node.atmos_sample.get_total_mols()
		_label_pressure_value.text = "%.02f" % _pipe_scanner_node.atmos_sample.pressure_kpa
		_label_temperature_value.text = "%.02f" % _pipe_scanner_node.atmos_sample.temperature_k
	else:
		_label_volume_value.text = "N/A"
		_label_network_id_value.text = "N/A"
		_label_total_mols_value.text = "N/A"
		_label_pressure_value.text = "N/A"
		_label_temperature_value.text  = "N/A"
