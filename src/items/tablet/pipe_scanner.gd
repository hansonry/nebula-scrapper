extends Node
class_name PipeScanner

@export var ray_cast : NodePath

@export var target_override: bool = false
@export var buildable_override: BuildableNode = null

@onready var _ray_cast_node : RayCast3D = get_node(ray_cast)

var _network : ExternalConnectorNetwork = null
var _volume : AtmosVolume = null
var _src_mutex : Mutex
var _dirty_flag : bool = true
var _has_volume : bool = false
var _dest_mutex : Mutex
var _dest_atmos_sample  : AtmosSample
var _volume_volume_l : float
var atmos_sample : AtmosSample = AtmosSample.new():
	get:
		if _has_volume:
			_dest_mutex.lock()
			if _dirty_flag:
				_dest_atmos_sample.copy(atmos_sample)
				atmos_sample.remove_below()
				_dirty_flag = false
			_dest_mutex.unlock()
			return atmos_sample
		return null
var volume_l : float = 0 :
	get:
		_dest_mutex.lock()
		volume_l = _volume_volume_l
		_dest_mutex.unlock()
		return volume_l
var network : ExternalConnectorNetwork = null:
	get:
		if _has_volume:
			_dest_mutex.lock()
			network = _network
			_dest_mutex.unlock()
			return network
		return null
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	assert(_ray_cast_node != null)
	_src_mutex = Mutex.new()
	_dest_mutex = Mutex.new()
	_dest_atmos_sample = AtmosSample.new()
	
	GameAtmosSystem.atmos_step_after.signal_connect(_after_atmos)
	


func _exit_tree():
	GameAtmosSystem.atmos_step_after.signal_disconnect(_after_atmos)

func _after_atmos():
	_src_mutex.lock()
	_dest_mutex.lock()
	if _volume != null:
		_dest_atmos_sample.set_from_volume(_volume)
		_volume_volume_l = _volume.volume_l
	else:
		_dest_atmos_sample.clear(true)
		_volume_volume_l = 0
	_dirty_flag = true
	_dest_mutex.unlock()
	_src_mutex.unlock()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	var found: bool = false
	var buildable_node : BuildableNode = null
	if target_override:
		buildable_node = buildable_override
	elif _ray_cast_node.is_colliding():
		buildable_node = BuildableNode.find(_ray_cast_node.get_collider())
	if buildable_node != null and buildable_node is SmallBuildableNode:
		var small_buildable: SmallBuildable = buildable_node.buildable
		if small_buildable.layer == SmallMap.Layer.PIPE:
			var vessel : Vessel = Vessel.find(buildable_node)
			assert(vessel != null)
			var info : Dictionary = vessel.get_pipe_information(buildable_node)
			_src_mutex.lock()
			_network = info[&"network"]
			_volume = info[&"volume"]
			_src_mutex.unlock()
			found = true
	if not found:
		_src_mutex.lock()
		_network = null
		_volume = null
		_src_mutex.unlock()
	_has_volume = found
	
		
