extends Container


@export var molecule: Molecule
@export var atmos_sample : AtmosSample

@onready var _lable_name : Label = %LabelName
@onready var _lable_mol_amount : Label = %LabelMolAmount

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	_lable_name.text = molecule.name
	_update_mol_data()


func _update_mol_data():
	if atmos_sample != null and atmos_sample.has_mols(molecule):
		_lable_mol_amount.text = "%f" % atmos_sample.get_mols(molecule)
		show()
	else:
		_lable_mol_amount.text = "N/A"
		hide()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	_update_mol_data()
