extends ItemViewNode
class_name ItemTabletView


@onready var _raycast_node : RayCast3D = $RayCast3D
@onready var _pipe_scanner: PipeScanner = $PipeScanner

var _hand : Character.Hand

func _set_default_hand_transform():
	rotate(Vector3(1, 0, 0), deg_to_rad(60))
	match _hand:
		Character.Hand.LEFT:
			translate(Vector3(0.24, 0.15, -0.2))
		Character.Hand.RIGHT:
			translate(Vector3(-0.24, 0.15, -0.2))
	

func target_override(node: BuildableNode):
	_pipe_scanner.buildable_override = node
	_pipe_scanner.target_override = true

func set_in_hand(hand : Character.Hand):
	_hand = hand
	_set_default_hand_transform()

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass
