extends ItemViewNode
class_name ToolItemViewNode

@export var hand_position : Vector3
@export var hand_rotation: Vector3

func set_in_hand(_hand : Character.Hand):
	var rot : Vector3 = Vector3(deg_to_rad(hand_rotation.x), deg_to_rad(hand_rotation.y), deg_to_rad(hand_rotation.z))
	
	transform = Transform3D(Basis.from_euler(rot), hand_position)

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass
