extends RigidBody3D
class_name ItemNode

const _material = preload("res://items/item_material.tres")
const _material_highlight = preload("res://items/highlight_material.tres")

@export var item_stack : ItemStack = null:
	set(value):
		if not is_node_ready():
			item_stack = value

@export var highlighted: bool = false:
	set(value):
		if highlighted != value:
			_set_highlight(value)
		highlighted = value


func _set_highlight(highlight: bool):
	for i in range(_mesh_list.size()):
		var mesh : MeshInstance3D = _mesh_list[i]
		if highlight:
			mesh.set_surface_override_material(0, _mesh_list_highlighted_materials[i])
		else:
			mesh.set_surface_override_material(0, null)

var _mesh_list : Array[MeshInstance3D] = []
var _mesh_list_highlighted_materials : Array[Material] = []



# Called when the node enters the scene tree for the first time.
func _ready():
	_find_important_things(self)
	_create_highlighted_materials()
	assert(_mesh_list.size() > 0)
	set_collision_layer(collision_layer | 3)


func _create_highlighted_materials():
	_mesh_list_highlighted_materials.resize(_mesh_list.size())
	for i in range(_mesh_list.size()):
		var original_material : Material = _mesh_list[i].get_active_material(0)
		var higlighted_material : Material = original_material.duplicate()
		higlighted_material.next_pass = _material_highlight
		_mesh_list_highlighted_materials[i] = higlighted_material
		
	
func _find_important_things(node : Node3D):
	if node is MeshInstance3D:
		_mesh_list.append(node)
	for child in node.get_children():
		if child is Node3D:
			_find_important_things(child)
		

func kill_self():
	set_collision_layer(0)
	queue_free()

func remove_from_stack(amount: int) -> bool:
	if amount < item_stack.count:
		item_stack.count -= amount
		return false
	kill_self()
	return true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
