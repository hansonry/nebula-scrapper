extends Resource
class_name Buildable

@export var preview_scene: PackedScene = null
@export var placement_script : GDScript = null
@export var step_list: Array[BuildStep] = []
@export var network_set: ConnectorNetworkSet


func make_preview_scene() -> Node3D:
	var obj : Node3D = preview_scene.instantiate()
	return obj

func make_building(index: int = 0) -> Node3D:
	return step_list[index].scene.instantiate()

func make_placement_script() -> Object:
	var obj: Object = placement_script.new()
	obj.set_buildable(self)
	return obj

func get_network_by_name(name: StringName) -> ConnectorNetwork:
	return network_set.get_network_by_name(name)

func has_network_type( type: ConnectorNetwork.Type) -> bool:
	return network_set.has_network_type(type)

func create_transformed_network_set(basis: Basis) -> ConnectorNetworkSet:
	return network_set.create_transformed_network_set(basis)
