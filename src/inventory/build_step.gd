extends Resource
class_name BuildStep

@export var scene : PackedScene = null
@export var assemble_tool : Array[StringName] = []
@export var disassemble_tool : Array[StringName] = []
@export var needed_material : Array[StringName] = []
@export var needed_material_amount : int = 0
@export var blocks_atmos : bool = false


func needs_tool_to_assemble() -> bool:
	return assemble_tool.size() > 0

func needs_material() -> bool:
	return needed_material_amount > 0
	
func can_assemble(active_hand: ItemStack, other_hand: ItemStack) -> bool:
	var n_material: bool = needs_material()
	var n_tool: bool = needs_tool_to_assemble()
	var material_hand : ItemStack = other_hand if n_tool else active_hand
	var has_tool : bool = not n_tool or (active_hand != null and TagUtil.is_at_least_one_tag_satisfied(assemble_tool, active_hand.item_type.tags))
	var has_material : bool = not n_material or (material_hand != null and TagUtil.is_at_least_one_tag_satisfied(needed_material, material_hand.item_type.tags) and
												 needed_material_amount <= material_hand.count)
	return has_tool and has_material
