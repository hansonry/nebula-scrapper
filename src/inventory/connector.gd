extends Resource
class_name Connector



@export var offset : Vector3i 
@export var direction: Vector3i 




func get_transformed_connector(basis: Basis) -> Connector:
	var out: Connector = Connector.new()
	out.offset = Vector3i(basis * Vector3(offset))
	out.direction = Vector3i(basis * Vector3(direction))
	return out

func get_target(basis:Basis = Basis.IDENTITY) -> Vector3i:
	return Vector3i(basis * Vector3(offset)) + Vector3i(basis * Vector3(direction))

func matches(b: Connector) -> bool:
	return offset == b.offset and direction == b.direction
