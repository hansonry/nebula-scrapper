@tool
extends Control
class_name ItemSlotUI

@onready var _texture_rect : TextureRect = $TextureRect
@onready var _label        : Label       = $Label


var _item_stack : ItemStack = null
var _item_type : ItemType = null

@export var inventory_group : InventoryGroup:
	set(value):
		var new_index = 0
		var info_change = false
		if value != null:
			new_index = clamp(inventory_index, 0 , value.size())
		if inventory_group != value:
			info_change = _inventory_change_to(value, new_index)
		inventory_group = value
		inventory_index = new_index
		if info_change:
			_update_item_type()
			_update_stack_count()

@export var inventory_index : int :
	set(value):
		var info_change = false
		value = clamp(value, 0, inventory_group.size())
		if inventory_index != value:
			info_change = _inventory_change_to(inventory_group, value)
		inventory_index = value
		if info_change:
			_update_item_type()
			_update_stack_count()

func clear_inventory():
	inventory_group = null
	inventory_index = 0

func set_inventory(new_inventory_group : InventoryGroup, new_inventory_index: int):
	inventory_group = new_inventory_group
	inventory_index = new_inventory_index

func has_stack() -> bool:
	return inventory_group.has_stack(inventory_index)

func get_item_stack() -> ItemStack:
	if inventory_group == null:
		return null
	return inventory_group.get_item_stack(inventory_index)

func _inventory_change_to(new_inventory_group : InventoryGroup, new_inventory_index: int):
	if new_inventory_group != inventory_group:
		if inventory_group != null:
			inventory_group.changed.disconnect(_on_invnetory_change)
		if new_inventory_group != null:
			new_inventory_group.changed.connect(_on_invnetory_change)
	
	# Check Item stacks
	var new_item_stack = null
	if new_inventory_group != null:
		new_item_stack = new_inventory_group.get_item_stack(new_inventory_index)
	if new_item_stack != _item_stack:
		if _item_stack != null:
			_item_stack.changed.disconnect(_on_item_stack_change)			
		if new_item_stack != null:
			new_item_stack.changed.connect(_on_item_stack_change)
		_item_stack = new_item_stack
		return true
	return false



func _on_invnetory_change():
	
	var new_item_stack = inventory_group.get_item_stack(inventory_index)
	if new_item_stack != _item_stack:
		if _item_stack != null:
			_item_stack.changed.disconnect(_on_item_stack_change)
		if new_item_stack != null:
			new_item_stack.changed.connect(_on_item_stack_change)
		_item_stack = new_item_stack
	_update_item_type()
	_update_stack_count()

func _on_item_stack_change():
	_update_item_type()
	_update_stack_count()

func _update_stack_count():
	if _label != null:
		if _item_stack == null or (_item_stack.item_type != null and _item_stack.item_type.max_stack == 1):
			_label.hide()
		else:
			_label.text = "%d" % _item_stack.count
			_label.show()

func _update_item_type():
	if _texture_rect != null:
		var to_item_type : ItemType = null
		var from_item_type : ItemType = _item_type
		if _item_stack != null:
			to_item_type = _item_stack.item_type
		
		if to_item_type != from_item_type:
			if _texture_rect.texture != null:
				_texture_rect.texture = null
			if to_item_type != null:
				_texture_rect.texture = to_item_type.icon_texture
			_item_type = to_item_type
			

func set_item_stack(stack: ItemStack) -> ItemStack:
	return inventory_group.set_item_stack(inventory_index, stack)

func _drop_data(_at_position : Vector2, data):
	if not Engine.is_editor_hint():
		var src = data.source
		var src_stack :ItemStack = data.stack
		if _item_stack != null and src_stack.item_type == _item_stack.item_type:
			var xfer_amount : int = min(_item_stack.free_space(), src_stack.count)
			if src is ItemSlotUI:
				src.remove_from_stack(xfer_amount)
			elif src is ItemNode:
				src.remove_from_stack(xfer_amount)
			_item_stack.count += xfer_amount
		else:
			if src is ItemSlotUI:
				var temp_stack = set_item_stack(null)
				src.set_item_stack(null)
				set_item_stack(src_stack)
				src.set_item_stack(temp_stack)
			elif src is ItemNode:
				src.kill_self()
				set_item_stack(src_stack)


func _get_drag_data(_at_position):
	if not Engine.is_editor_hint() and _item_stack != null and _item_stack.item_type != null:
		return _item_stack.get_drag_info(self)
	return null


func _can_drop_data(_at_position: Vector2, data) -> bool:
	if Engine.is_editor_hint() or typeof(data) != TYPE_DICTIONARY or not data.has("type"):
		return false
	if data.type == "ItemStack":
		if data.source is ItemSlotUI:
			return true
		if data.source is ItemNode and (_item_stack == null or 
		   (_item_stack.item_type == data.stack.item_type and not _item_stack.is_full())):
			return true
	return false

# Called when the node enters the scene tree for the first time.
func _ready():
	if not Engine.is_editor_hint():
		_update_item_type()
		_update_stack_count()


func remove_from_stack(amount : int):
	if amount < _item_stack.count:
		_item_stack.count -= amount
	else:
		set_item_stack(null)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
