extends Resource
class_name ItemType

@export var name : String = "":
	set(value):
		name = value
		changed.emit()
@export var max_stack : int = 1:
	set(value):
		max_stack = value
		changed.emit()
@export var icon_texture : Texture2D = null:
	set(value):
		icon_texture = value
		changed.emit()
@export var view_scene : PackedScene = null:
	set(value):
		view_scene = value
		changed.emit()
@export var physics_scene : PackedScene = null:
	set(value):
		physics_scene = value
		changed.emit()
@export var buildables : Array[Buildable] = []:
	set(value):
		buildables = value
		changed.emit()
@export var tags : Array[StringName] = []

func make_placement() -> Placement:
	var placement : Placement = Placement.new()
	placement.buildables = buildables
	return placement

func has_tag(tag: StringName) -> bool:
	return tags.has(tag)
