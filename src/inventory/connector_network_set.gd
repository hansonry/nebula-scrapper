extends Resource
class_name ConnectorNetworkSet

@export var network_list : Array[ConnectorNetwork]

func get_network_by_name(name: StringName) -> ConnectorNetwork:
	for n: ConnectorNetwork in network_list:
		if n.name == name:
			return n
	return null

func has_network_type( type: ConnectorNetwork.Type) -> bool:
	for n: ConnectorNetwork in network_list:
		if n.type == type:
			return true
	return false

func create_transformed_network_set(basis: Basis) -> ConnectorNetworkSet:
	var xform_set : ConnectorNetworkSet = ConnectorNetworkSet.new()
	xform_set.network_list.resize(network_list.size())
	for i: int in range(network_list.size()):
		var cn: ConnectorNetwork = network_list[i]
		xform_set.network_list[i] = cn.create_transformed_copy(basis)
	return xform_set

func assert_check():
	for n: ConnectorNetwork in network_list:
		n.assert_check()
