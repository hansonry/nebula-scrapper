extends Resource
class_name ConnectorNetwork

enum Type {PIPE, WIRE}

@export var name : StringName
@export var type : Type
@export var connector_list : Array[Connector]


func create_transformed_connector_list(basis: Basis) -> Array[Connector]:
	var out: Array[Connector]
	out.resize(connector_list.size())
	for i in range(connector_list.size()):
		out[i] = connector_list[i].get_transformed_connector(basis)
	return out


func create_transformed_copy(basis: Basis) -> ConnectorNetwork:
	var cn : ConnectorNetwork = ConnectorNetwork.new()
	cn.name = name
	cn.type = type
	cn.connector_list = create_transformed_connector_list(basis)
	return cn

func assert_check():
	for ci1: int in range(connector_list.size() - 1):
		for ci2: int in range(ci1 + 1, connector_list.size()):
			assert(not connector_list[ci1].matches(connector_list[ci2]), "Duplicate connectors")
		
