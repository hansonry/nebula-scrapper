@tool
extends Resource
class_name InventoryGroup

signal slot_changed(index: int)

@export var filter : Array[String] = []

var _item_slots: Array[ItemStack] = [null]
var _callable: Array[Callable] = []

func _init(_size : int = 1):
	_item_slots.resize(_size)
	_callable.resize(_size)
	
func size() -> int:
	return _item_slots.size()
	
func get_item_stack(index: int) -> ItemStack:
	if _item_slots == null:
		return null
	if index < 0 or index >= _item_slots.size():
		return null
	return _item_slots[index]

func set_item_stack(index: int, stack: ItemStack) -> ItemStack:
	if _item_slots != null and index >= 0 and index < _item_slots.size():
		var prev_slot = _item_slots[index]
		_item_slots[index] = stack
		if prev_slot != stack:
			if prev_slot != null:
				prev_slot.changed.disconnect(_callable[index])
			if stack != null:
				var temp_call: Callable = Callable(_item_stack_changed)
				_callable[index] = temp_call.bind(index)
				stack.changed.connect(_callable[index])
			changed.emit()
			slot_changed.emit(index)
		return prev_slot
	return null

func _item_stack_changed( index: int):
	slot_changed.emit(index)


func has_stack(index: int) -> bool:
	return _item_slots != null and index >= 0 and index < _item_slots.size() and _item_slots[index] != null
	
func change_stack_amount(index : int, delta : int):
	if has_stack(index):
		var current_amount = _item_slots[index].count
		var new_amount = current_amount + delta
		if new_amount <= 0:
			set_item_stack(index, null)
		else:
			_item_slots[index].count = new_amount

func find_index_of_item_stack(stack :ItemStack) -> int:
	for index in _item_slots.size():
		if _item_slots[index] == stack:
			return index
	return -1
