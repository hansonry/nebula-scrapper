@tool
extends Resource
class_name ItemStack

const _item_node_script = preload("res://items/item_node.gd")
const _item_view_node_script = preload("res://items/item_view_node.gd")

@export var item_type: ItemType = null :
	set(value):
		item_type = value
		if item_type != null:
			count = clamp(count, 1, item_type.max_stack)
		else:
			count = 1
		changed.emit()
@export var count: int = 1:
	set(value):
		if item_type == null:
			count = 1
		else:
			count = clamp(value, 1, item_type.max_stack)
		changed.emit()


func create_item_node() -> ItemNode:
	var node = item_type.physics_scene.instantiate()
	var supplied_script : Script = node.get_script()
	
	if supplied_script == null:
		node.set_script(_item_node_script)
	var item_node : ItemNode = node
	item_node.item_stack = self
	return item_node

func create_view_node() -> ItemViewNode:
	var node: Node3D = item_type.view_scene.instantiate()
	var supplied_script : Script = node.get_script()
	if supplied_script == null:
		node.set_script(_item_view_node_script)
	var item_view_node : ItemViewNode = node
	item_view_node.item_stack = self
	return item_view_node

func get_drag_info(control: Control, source_object = null, texture_size = Vector2(64, 64)):
	if source_object == null:
		source_object = control
	var data = {type = "ItemStack", stack = self, source = source_object}
	var preview_texture = TextureRect.new()
	preview_texture.expand_mode = TextureRect.EXPAND_FIT_HEIGHT_PROPORTIONAL
	preview_texture.stretch_mode = TextureRect.STRETCH_KEEP_ASPECT
	preview_texture.texture = item_type.icon_texture
	preview_texture.size = texture_size
	preview_texture.position = -preview_texture.size / 2
	var preview = Control.new()
	preview.add_child(preview_texture)
	control.set_drag_preview(preview)
	return data

func split(amount: int) -> ItemStack:
	assert(amount < count)
	var new_stack :ItemStack = ItemStack.new()
	new_stack.item_type = item_type
	count -= amount
	new_stack.count = amount
	return new_stack
	
func free_space() -> int:
	return item_type.max_stack - count
func is_full() -> bool:
	return count >= item_type.max_stack
