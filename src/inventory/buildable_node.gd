extends Node3D
class_name BuildableNode

signal step_changed

@export var buildable :Buildable = null
var _network_set: ConnectorNetworkSet = null

@export var network_set : ConnectorNetworkSet:
	get:
		if _network_set == null:
			_network_set = buildable.create_transformed_network_set(basis)
		return _network_set

		
var _materials: Array[ItemStack] = [null]
var _step : int = -1
var step: int :
	get:
		return _step

func _set_step(value : int):
	if value != _step:
		assert(buildable != null)
		_materials.resize(value + 1)
		if _node != null:
			_node.queue_free()
		if value == -1 and _step == 0:
			_step = 0
			_remove_from_grid()
			queue_free()
		else:
			_step = value
			_node = buildable.make_building(_step)
			add_child(_node)
			step_changed.emit()

func set_inital_items(stack : ItemStack):
	_materials[0] = stack

func assemble(stack: ItemStack):
	var index : int = _step + 1
	_set_step(index)
	_materials[index] = stack

func disassemble() -> ItemStack:
	var index : int = _step - 1
	var item_stack : ItemStack = _materials[_step]
	_set_step(index)
	return item_stack


func _remove_from_grid():
	assert(false)

var _node : Node3D = null


func get_current_step() -> BuildStep:
	return buildable.step_list[_step]

func get_next_step() -> BuildStep:
	var index :int = _step + 1
	if index < buildable.step_list.size():
		return buildable.step_list[index]
	return null

func has_connection_with(node : BuildableNode, type: ConnectorNetwork.Type) -> bool:
	var my_local : Vector3i = SmallMap.small_position_from_local(position)
	var other_local : Vector3i = SmallMap.small_position_from_local(node.position)
	for cn1 : ConnectorNetwork in network_set.network_list:
		if cn1.type == type:
			for cn2 : ConnectorNetwork in node.network_set.network_list:
				if cn2.type == type:
					for c1 : Connector in cn1.connector_list:
						var p1 : Vector3i = my_local +  c1.offset
						var t1 : Vector3i = p1 + c1.direction
						for c2: Connector in cn2.connector_list:
							var p2 : Vector3i = other_local + c2.offset
							var t2 : Vector3i = p2 + c2.direction
							if t1 == p2 and t2 == p1 and (c1.direction + c2.direction) == Vector3i.ZERO:
								return true
	return false

# Called when the node enters the scene tree for the first time.
func _ready():
	if _step == -1:
		_set_step(0)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func has_network_type(type : ConnectorNetwork.Type) -> bool:
	return network_set.has_network_type(type)

static func find(node: Node3D) -> BuildableNode:
	while node != null and not node is BuildableNode:
		node = node.get_parent_node_3d()
	return node
