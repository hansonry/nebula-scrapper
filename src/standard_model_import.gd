@tool # Needed so it runs in editor.
extends EditorScenePostImport

const _material = preload("res://items/item_material.tres")

# This sample changes all node names.
# Called right after the scene is imported and gets the root node.
func _post_import(scene):
	var mesh3d = find_mesh_instance_3d(scene)
	if mesh3d == null:
		print("Failed to find Mesh 3d")
		return scene # Remember to return the imported scene
	mesh3d.transform = Transform3D.IDENTITY
	mesh3d.mesh.surface_set_material(0, _material)
	return mesh3d


func find_mesh_instance_3d(scene : Node3D) -> MeshInstance3D:
	if scene is MeshInstance3D:
		return scene
	for child in scene.get_children():
		var result = find_mesh_instance_3d(child)
		if result != null:
			return result
	return null
